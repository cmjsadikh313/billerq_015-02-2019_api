-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 15, 2019 at 06:45 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `billerq_07-02-2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `barcodes`
--

DROP TABLE IF EXISTS `barcodes`;
CREATE TABLE IF NOT EXISTS `barcodes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `width` double(8,2) DEFAULT NULL,
  `height` double(8,2) DEFAULT NULL,
  `paper_width` double(8,2) DEFAULT NULL,
  `paper_height` double(8,2) DEFAULT NULL,
  `top_margin` double(8,2) DEFAULT NULL,
  `left_margin` double(8,2) DEFAULT NULL,
  `row_distance` double(8,2) DEFAULT NULL,
  `col_distance` double(8,2) DEFAULT NULL,
  `stickers_in_one_row` int(11) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_continuous` tinyint(1) NOT NULL DEFAULT '0',
  `stickers_in_one_sheet` int(11) DEFAULT NULL,
  `business_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `barcodes_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barcodes`
--

INSERT INTO `barcodes` (`id`, `name`, `description`, `width`, `height`, `paper_width`, `paper_height`, `top_margin`, `left_margin`, `row_distance`, `col_distance`, `stickers_in_one_row`, `is_default`, `is_continuous`, `stickers_in_one_sheet`, `business_id`, `created_at`, `updated_at`) VALUES
(1, '20 Labels per Sheet - (8.5\" x 11\")', 'Sheet Size: 8.5\" x 11\"\\r\\nLabel Size: 4\" x 1\"\\r\\nLabels per sheet: 20', 3.75, 1.00, 8.50, 11.00, 0.50, 0.50, 0.00, 0.16, 2, 0, 0, 20, NULL, '2017-12-18 00:43:44', '2017-12-18 00:43:44'),
(2, '30 Labels per sheet - (8.5\" x 11\")', 'Sheet Size: 8.5\" x 11\"\\r\\nLabel Size: 2.625\" x 1\"\\r\\nLabels per sheet: 30', 2.62, 1.00, 8.50, 11.00, 0.50, 0.22, 0.00, 0.14, 3, 0, 0, 30, NULL, '2017-12-18 00:34:39', '2017-12-18 00:40:40'),
(3, '32 Labels per sheet - (8.5\" x 11\")', 'Sheet Size: 8.5\" x 11\"\\r\\nLabel Size: 2\" x 1.25\"\\r\\nLabels per sheet: 32', 2.00, 1.25, 8.50, 11.00, 0.50, 0.25, 0.00, 0.00, 4, 0, 0, 32, NULL, '2017-12-18 00:25:40', '2017-12-18 00:25:40'),
(4, '40 Labels per sheet - (8.5\" x 11\")', 'Sheet Size: 8.5\" x 11\"\\r\\nLabel Size: 2\" x 1\"\\r\\nLabels per sheet: 40', 2.00, 1.00, 8.50, 11.00, 0.50, 0.25, 0.00, 0.00, 4, 0, 0, 40, NULL, '2017-12-18 00:28:40', '2017-12-18 00:28:40'),
(5, '50 Labels per Sheet - (8.5\" x 11\")', 'Sheet Size: 8.5\" x 11\"\\r\\nLabel Size: 1.5\" x 1\"\\r\\nLabels per sheet: 50', 1.50, 1.00, 8.50, 11.00, 0.50, 0.50, 0.00, 0.00, 5, 0, 0, 50, NULL, '2017-12-18 00:21:10', '2017-12-18 00:21:10'),
(6, 'Continuous Rolls - 31.75mm x 25.4mm', 'Label Size: 31.75mm x 25.4mm\\r\\nGap: 3.18mm', 1.25, 1.00, 1.25, 0.00, 0.12, 0.00, 0.12, 0.00, 1, 0, 1, NULL, NULL, '2017-12-18 00:21:10', '2017-12-18 00:21:10');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_id` int(10) UNSIGNED NOT NULL,
  `waiter_id` int(10) UNSIGNED DEFAULT NULL,
  `table_id` int(10) UNSIGNED DEFAULT NULL,
  `correspondent_id` int(11) DEFAULT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `booking_start` datetime NOT NULL,
  `booking_end` datetime NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `booking_status` enum('booked','completed','cancelled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking_note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bookings_contact_id_foreign` (`contact_id`),
  KEY `bookings_business_id_foreign` (`business_id`),
  KEY `bookings_created_by_foreign` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brands_business_id_foreign` (`business_id`),
  KEY `brands_created_by_foreign` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `business_id`, `name`, `description`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 3, 'coship', 'ciship set top box', 3, NULL, '2018-11-28 06:13:48', '2018-11-28 06:13:48'),
(2, 4, 'orkus', NULL, 5, NULL, '2018-11-28 08:14:23', '2018-11-28 08:14:23'),
(3, 4, 'philips', NULL, 5, NULL, '2018-11-30 11:35:10', '2018-11-30 11:35:10'),
(4, 7, 'Abu Jamal', NULL, 9, NULL, '2018-12-13 11:58:21', '2018-12-13 11:58:21'),
(5, 7, 'أنكور.   Anchor', NULL, 9, NULL, '2018-12-13 13:49:04', '2018-12-13 13:49:04'),
(6, 2, 'brnd123', 'brndddddd', 2, NULL, '2018-12-17 07:52:17', '2018-12-17 07:54:02'),
(7, 16, 'Allen Solly', 'Allen Solly cloths', 19, NULL, '2019-01-16 11:17:40', '2019-01-16 11:17:40'),
(8, 16, 'Surplus', 'cloths', 19, NULL, '2019-01-16 11:24:19', '2019-01-16 11:24:19'),
(9, 16, 'Samsung', 'samsung mobile', 19, NULL, '2019-01-16 11:26:45', '2019-01-16 11:26:45'),
(10, 2, 'Demo brand', 'sds', 2, NULL, '2019-01-25 09:14:47', '2019-01-25 09:14:47'),
(11, 17, 'Samsung', 'samsung  brand descr', 22, NULL, '2019-02-08 05:40:16', '2019-02-08 05:40:16'),
(12, 17, 'Apple', 'descrpton', 22, NULL, '2019-02-08 05:40:46', '2019-02-08 05:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

DROP TABLE IF EXISTS `business`;
CREATE TABLE IF NOT EXISTS `business` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` int(10) UNSIGNED NOT NULL,
  `start_date` date DEFAULT NULL,
  `tax_number_1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_label_1` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_number_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_label_2` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_sales_tax` int(10) UNSIGNED DEFAULT NULL,
  `default_profit_percent` double(5,2) NOT NULL DEFAULT '0.00',
  `owner_id` int(10) UNSIGNED NOT NULL,
  `time_zone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Asia/Kolkata',
  `fy_start_month` tinyint(4) NOT NULL DEFAULT '1',
  `accounting_method` enum('fifo','lifo','avco') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'fifo',
  `default_sales_discount` decimal(20,2) DEFAULT NULL,
  `sell_price_tax` enum('includes','excludes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'includes',
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `expiry_type` enum('add_expiry','add_manufacturing') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'add_expiry',
  `on_product_expiry` enum('keep_selling','stop_selling','auto_delete') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'keep_selling',
  `stop_selling_before` int(11) NOT NULL COMMENT 'Stop selling expied item n days before expiry',
  `enable_tooltip` tinyint(1) NOT NULL DEFAULT '1',
  `purchase_in_diff_currency` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Allow purchase to be in different currency then the business currency',
  `purchase_currency_id` int(10) UNSIGNED DEFAULT NULL,
  `p_exchange_rate` decimal(20,3) NOT NULL DEFAULT '1.000',
  `transaction_edit_days` int(10) UNSIGNED NOT NULL DEFAULT '30',
  `stock_expiry_alert_days` int(10) UNSIGNED NOT NULL DEFAULT '30',
  `keyboard_shortcuts` text COLLATE utf8mb4_unicode_ci,
  `pos_settings` text COLLATE utf8mb4_unicode_ci,
  `enable_brand` tinyint(1) NOT NULL DEFAULT '1',
  `enable_category` tinyint(1) NOT NULL DEFAULT '1',
  `enable_sub_category` tinyint(1) NOT NULL DEFAULT '1',
  `enable_price_tax` tinyint(1) NOT NULL DEFAULT '1',
  `enable_purchase_status` tinyint(1) DEFAULT '1',
  `enable_lot_number` tinyint(1) NOT NULL DEFAULT '0',
  `default_unit` int(11) DEFAULT NULL,
  `enable_racks` tinyint(1) NOT NULL DEFAULT '0',
  `enable_row` tinyint(1) NOT NULL DEFAULT '0',
  `enable_position` tinyint(1) NOT NULL DEFAULT '0',
  `enable_editing_product_from_purchase` tinyint(1) NOT NULL DEFAULT '1',
  `sales_cmsn_agnt` enum('logged_in_user','user','cmsn_agnt') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_addition_method` tinyint(1) NOT NULL DEFAULT '1',
  `enable_inline_tax` tinyint(1) NOT NULL DEFAULT '1',
  `currency_symbol_placement` enum('before','after') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'before',
  `enabled_modules` text COLLATE utf8mb4_unicode_ci,
  `date_format` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'm/d/Y',
  `time_format` enum('12','24') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '24',
  `ref_no_prefixes` text COLLATE utf8mb4_unicode_ci,
  `theme_color` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `email_settings` text COLLATE utf8mb4_unicode_ci,
  `sms_settings` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `business_owner_id_foreign` (`owner_id`),
  KEY `business_currency_id_foreign` (`currency_id`),
  KEY `business_default_sales_tax_foreign` (`default_sales_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `name`, `currency_id`, `start_date`, `tax_number_1`, `tax_label_1`, `tax_number_2`, `tax_label_2`, `default_sales_tax`, `default_profit_percent`, `owner_id`, `time_zone`, `fy_start_month`, `accounting_method`, `default_sales_discount`, `sell_price_tax`, `logo`, `sku_prefix`, `enable_product_expiry`, `expiry_type`, `on_product_expiry`, `stop_selling_before`, `enable_tooltip`, `purchase_in_diff_currency`, `purchase_currency_id`, `p_exchange_rate`, `transaction_edit_days`, `stock_expiry_alert_days`, `keyboard_shortcuts`, `pos_settings`, `enable_brand`, `enable_category`, `enable_sub_category`, `enable_price_tax`, `enable_purchase_status`, `enable_lot_number`, `default_unit`, `enable_racks`, `enable_row`, `enable_position`, `enable_editing_product_from_purchase`, `sales_cmsn_agnt`, `item_addition_method`, `enable_inline_tax`, `currency_symbol_placement`, `enabled_modules`, `date_format`, `time_format`, `ref_no_prefixes`, `theme_color`, `created_by`, `email_settings`, `sms_settings`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'BillerQ', 53, '2018-11-14', NULL, NULL, NULL, NULL, NULL, 35.00, 1, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', 'zxiFZQJuxLd8eaV9T1LgvLSv2eP6tsajzmb1D1QG.png', NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"recent_product_quantity\":\"f2\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"add_new_product\":\"f4\"}}', '{\"disable_pay_checkout\":0,\"disable_draft\":0,\"disable_express_checkout\":0,\"hide_product_suggestion\":0,\"hide_recent_trans\":0,\"disable_discount\":0,\"disable_order_tax\":0,\"is_pos_subtotal_editable\":0}', 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '12', '{\"purchase\":\"PO\",\"purchase_return\":null,\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"expense_payment\":null,\"business_location\":\"BL\"}', 'blue-light', NULL, '{\"mail_host\":null,\"mail_port\":null,\"mail_username\":\"superadmin\",\"mail_password\":\"<?quad?>\",\"mail_encryption\":null,\"mail_from_address\":null,\"mail_from_name\":null}', '{\"url\":null,\"send_to_param_name\":\"to\",\"msg_param_name\":\"text\",\"request_method\":\"post\",\"param_1\":null,\"param_val_1\":null,\"param_2\":null,\"param_val_2\":null,\"param_3\":null,\"param_val_3\":null,\"param_4\":null,\"param_val_4\":null,\"param_5\":null,\"param_val_5\":null}', 1, '2018-11-14 07:52:38', '2018-12-26 13:17:51'),
(2, 'Billbix Demo', 53, '2018-11-26', NULL, NULL, NULL, NULL, NULL, 25.00, 2, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', 'AJauBLTrDWtbjWG7ybZqZVFpdyoDAELEoQcInEHC.png', NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"recent_product_quantity\":\"f2\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"add_new_product\":\"f4\"}}', '{\"disable_pay_checkout\":0,\"disable_draft\":0,\"disable_express_checkout\":0,\"hide_product_suggestion\":0,\"hide_recent_trans\":0,\"disable_discount\":0,\"disable_order_tax\":0,\"is_pos_subtotal_editable\":0}', 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"purchase_return\":null,\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"expense_payment\":null,\"business_location\":\"BL\"}', 'purple-light', NULL, '{\"mail_host\":null,\"mail_port\":null,\"mail_username\":null,\"mail_password\":null,\"mail_encryption\":null,\"mail_from_address\":null,\"mail_from_name\":null}', '{\"url\":null,\"send_to_param_name\":\"to\",\"msg_param_name\":\"text\",\"request_method\":\"post\",\"param_1\":null,\"param_val_1\":null,\"param_2\":null,\"param_val_2\":null,\"param_3\":null,\"param_val_3\":null,\"param_4\":null,\"param_val_4\":null,\"param_5\":null,\"param_val_5\":null}', 1, '2018-11-26 12:37:30', '2018-11-27 13:52:47'),
(3, 'Cable Nine', 53, '2018-11-27', NULL, NULL, NULL, NULL, NULL, 25.00, 3, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', 'SO7Rk6wcTXi2Ngp3xSnMl3h2kBj46RnkmnQyGJsN.jpeg', NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-11-27 14:32:23', '2018-11-28 06:22:47'),
(4, 'Ashwin Johny', 53, '2018-11-28', NULL, NULL, NULL, NULL, NULL, 25.00, 5, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(6, 'Apsara Bakes', 53, '2018-12-03', NULL, NULL, NULL, NULL, NULL, 25.00, 8, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', 'RDCYPdTgOTmyKkeDFSUPDQOylQqllxPdBr8Q5Z3L.png', NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(7, 'nazer', 101, '2018-12-11', NULL, NULL, NULL, NULL, NULL, 25.00, 9, 'Asia/Riyadh', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(9, 'deepu', 90, '2018-12-11', NULL, NULL, NULL, NULL, NULL, 25.00, 11, 'Asia/Muscat', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(10, 'Shamnas', 136, '2018-12-12', NULL, NULL, NULL, NULL, NULL, 25.00, 12, 'Asia/Dubai', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-12 13:09:05', '2018-12-12 12:13:42'),
(11, 'Novyy Technologies', 53, '2018-12-20', NULL, NULL, NULL, NULL, NULL, 25.00, 13, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(12, 'demo1111111', 53, '2018-12-27', NULL, NULL, NULL, NULL, NULL, 25.00, 14, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', 'PZyN2k19AwYOU59qGIyOZDHK4yPs9qPzAkyhxXrK.jpeg', NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-28 10:56:24', '2018-12-28 10:56:24'),
(13, 'CM.Jaefar sadikh', 53, '2018-12-27', NULL, NULL, NULL, NULL, NULL, 25.00, 15, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(14, 'CM.Jaefar sadikh', 16, '2018-12-27', NULL, NULL, NULL, NULL, NULL, 25.00, 16, 'Africa/Bissau', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-28 12:10:11', '2018-12-28 12:10:11'),
(15, 'jaefar', 53, '2018-12-27', NULL, NULL, NULL, NULL, NULL, 25.00, 17, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2018-12-28 12:21:08', '2018-12-28 12:21:08'),
(16, 'sadikh jaefar', 53, '2019-01-16', NULL, NULL, NULL, NULL, NULL, 25.00, 19, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', 'vMZGhFGFej3wTgOlAAVO449FYJMigHdQLOYe31E7.png', NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2019-01-16 07:38:32', '2019-01-16 07:38:32'),
(17, 'skilz', 53, '2019-02-07', NULL, NULL, NULL, NULL, NULL, 25.00, 22, 'Asia/Kolkata', 1, 'fifo', NULL, 'includes', NULL, NULL, 0, 'add_expiry', 'keep_selling', 0, 1, 0, NULL, '1.000', 30, 30, '{\"pos\":{\"express_checkout\":\"shift+e\",\"pay_n_ckeckout\":\"shift+p\",\"draft\":\"shift+d\",\"cancel\":\"shift+c\",\"edit_discount\":\"shift+i\",\"edit_order_tax\":\"shift+t\",\"add_payment_row\":\"shift+r\",\"finalize_payment\":\"shift+f\",\"recent_product_quantity\":\"f2\",\"add_new_product\":\"f4\"}}', NULL, 1, 1, 1, 1, 1, 0, NULL, 0, 0, 0, 1, NULL, 1, 0, 'before', NULL, 'm/d/Y', '24', '{\"purchase\":\"PO\",\"stock_transfer\":\"ST\",\"stock_adjustment\":\"SA\",\"sell_return\":\"CN\",\"expense\":\"EP\",\"contacts\":\"CO\",\"purchase_payment\":\"PP\",\"sell_payment\":\"SP\",\"business_location\":\"BL\"}', NULL, NULL, NULL, NULL, 1, '2019-02-08 05:33:52', '2019-02-08 05:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `business_locations`
--

DROP TABLE IF EXISTS `business_locations`;
CREATE TABLE IF NOT EXISTS `business_locations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `location_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landmark` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_scheme_id` int(10) UNSIGNED NOT NULL,
  `invoice_layout_id` int(10) UNSIGNED NOT NULL,
  `print_receipt_on_invoice` tinyint(1) DEFAULT '1',
  `receipt_printer_type` enum('browser','printer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'browser',
  `printer_id` int(11) DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `business_locations_business_id_index` (`business_id`),
  KEY `business_locations_invoice_scheme_id_foreign` (`invoice_scheme_id`),
  KEY `business_locations_invoice_layout_id_foreign` (`invoice_layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_locations`
--

INSERT INTO `business_locations` (`id`, `business_id`, `location_id`, `name`, `landmark`, `country`, `state`, `city`, `zip_code`, `invoice_scheme_id`, `invoice_layout_id`, `print_receipt_on_invoice`, `receipt_printer_type`, `printer_id`, `mobile`, `alternate_number`, `email`, `website`, `custom_field1`, `custom_field2`, `custom_field3`, `custom_field4`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'BL0001', 'Billbix', 'UL Cyberpark', 'India', 'Kerala', 'Calicut', '673016', 1, 1, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(2, 2, 'BL0001', 'Billbix Demo', 'ULCP', 'India', 'Kerala', 'Kozhikode', '673016', 2, 2, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(3, 3, 'BL0001', 'Cable Nine', 'Focus mall', 'India', 'Kerala', 'Kozhikode', '673001', 3, 3, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(4, 3, '2', 'Cable nine', 'Lulu mall', 'India', 'Kerala', 'cochim', '682005', 3, 3, 1, 'browser', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-27 14:40:01', '2018-11-27 14:40:22'),
(5, 4, 'BL0001', 'Ashwin Johny', 'Thrissur', 'India', 'Kerala', 'Thrissur', '11111', 4, 4, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(6, 4, 'po', 'aloor', 'po', 'India', 'Kerala', 'thrissur', '680001', 4, 4, 1, 'browser', NULL, '8281186836', NULL, 'ashwinjohny@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-30 11:29:36', '2018-11-30 11:29:36'),
(8, 6, 'BL0001', 'Apsara Bakes', 'Keezhurkunnu', 'India', 'Kerala', 'Iritty', '670703', 6, 6, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(9, 7, 'BL0001', 'nazer', 'xx', 'soudi Arabia', 'xx', 'xx', '111', 7, 7, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(11, 9, 'BL0001', 'deepu', 'oman', 'oman', 'Dhofar', 'dhofar', '217', 9, 9, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(12, 10, 'BL0001', 'Shamnas', 'dubai mall', 'UAE', 'dubai', 'dubai', '1122', 10, 10, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(13, 11, 'BL0001', 'Novyy Technologies', 'Ul Cyber Park', 'India', 'Kerala', 'Kozhikode', '673016', 11, 11, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(14, 12, 'BL0001', 'demo1111111', 'ulcyber', 'India', 'kerala', 'Tirur', '676767', 12, 12, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(15, 13, 'BL0001', 'CM.Jaefar sadikh', 'ulcyber', 'UAE', 'kerala', 'kozhikode', '676767', 13, 13, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(16, 14, 'BL0001', 'CM.Jaefar sadikh', 'ul', 'UAE', 'kerala', 'india', '676767', 14, 14, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(17, 15, 'BL0001', 'jaefar', 'ul', 'UAE', 'kerala', 'india', '676767', 15, 15, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(18, 16, 'BL0001', 'sadikh jaefar', 'ulcp', 'india', 'kerala', 'kozhikode', '676767', 16, 16, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(19, 17, 'BL0001', 'skilz', 'tirur', 'india', 'kerela', 'kozhikode', '67633', 17, 17, 1, 'browser', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-08 05:33:52', '2019-02-08 05:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `cash_registers`
--

DROP TABLE IF EXISTS `cash_registers`;
CREATE TABLE IF NOT EXISTS `cash_registers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('close','open') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `closed_at` datetime DEFAULT NULL,
  `closing_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `total_card_slips` int(11) NOT NULL DEFAULT '0',
  `total_cheques` int(11) NOT NULL DEFAULT '0',
  `closing_note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cash_registers_business_id_foreign` (`business_id`),
  KEY `cash_registers_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cash_registers`
--

INSERT INTO `cash_registers` (`id`, `business_id`, `user_id`, `status`, `closed_at`, `closing_amount`, `total_card_slips`, `total_cheques`, `closing_note`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'open', NULL, '0.00', 0, 0, NULL, '2018-11-26 12:50:35', '2018-11-26 12:50:35'),
(2, 3, 4, 'close', '2018-11-28 10:48:45', '0.00', 0, 0, NULL, '2018-11-28 05:18:27', '2018-11-28 05:18:45'),
(3, 3, 4, 'close', '2018-11-28 11:20:31', '0.00', 0, 0, NULL, '2018-11-28 05:19:09', '2018-11-28 05:50:31'),
(4, 4, 5, 'close', '2018-11-28 13:49:50', '100.00', 0, 0, NULL, '2018-11-28 08:01:46', '2018-11-28 08:19:50'),
(5, 3, 4, 'open', NULL, '0.00', 0, 0, NULL, '2018-11-28 09:42:14', '2018-11-28 09:42:14'),
(6, 4, 5, 'open', NULL, '0.00', 0, 0, NULL, '2018-11-30 11:30:15', '2018-11-30 11:30:15'),
(7, 10, 12, 'open', NULL, '0.00', 0, 0, NULL, '2018-12-13 13:06:09', '2018-12-13 13:06:09'),
(8, 15, 18, 'open', NULL, '0.00', 0, 0, NULL, '2018-12-29 07:48:10', '2018-12-29 07:48:10'),
(9, 16, 20, 'open', NULL, '0.00', 0, 0, NULL, '2019-01-16 09:29:07', '2019-01-16 09:29:07'),
(10, 16, 21, 'open', NULL, '0.00', 0, 0, NULL, '2019-01-16 10:08:32', '2019-01-16 10:08:32'),
(11, 17, 22, 'open', NULL, '0.00', 0, 0, NULL, '2019-02-08 05:38:11', '2019-02-08 05:38:11'),
(12, 17, 24, 'close', '2019-02-11 12:39:49', '10000.00', 0, 0, NULL, '2019-02-11 07:04:45', '2019-02-11 07:09:49'),
(13, 17, 24, 'close', '2019-02-11 13:22:06', '1444.00', 0, 0, NULL, '2019-02-11 07:44:25', '2019-02-11 07:52:06'),
(14, 17, 24, 'close', '2019-02-11 13:22:06', '1444.00', 0, 0, NULL, '2019-02-11 07:45:53', '2019-02-11 07:52:06'),
(15, 17, 24, 'close', '2019-02-11 13:22:06', '1444.00', 0, 0, NULL, '2019-02-11 07:46:23', '2019-02-11 07:52:06'),
(16, 17, 24, 'close', '2019-02-11 13:22:06', '1444.00', 0, 0, NULL, '2019-02-11 07:46:29', '2019-02-11 07:52:06'),
(17, 17, 24, 'close', '2019-02-11 13:22:06', '1444.00', 0, 0, NULL, '2019-02-11 07:48:33', '2019-02-11 07:52:06'),
(18, 17, 24, 'close', '2019-02-11 13:28:11', '10007.00', 0, 0, NULL, '2019-02-11 07:52:23', '2019-02-11 07:58:11'),
(19, 17, 24, 'close', '2019-02-11 15:13:45', '64519.00', 0, 0, NULL, '2019-02-11 09:05:21', '2019-02-11 09:43:45'),
(20, 17, 24, 'close', '2019-02-11 15:14:38', '10000.00', 0, 0, NULL, '2019-02-11 09:44:01', '2019-02-11 09:44:38'),
(21, 17, 24, 'close', '2019-02-11 15:31:48', '1.00', 0, 0, 'noooo', '2019-02-11 09:45:09', '2019-02-11 10:01:48'),
(22, 17, 24, 'close', '2019-02-11 15:51:17', '1.00', 0, 0, 'nooo', '2019-02-11 10:04:27', '2019-02-11 10:21:17'),
(23, 17, 24, 'close', '2019-02-11 15:51:17', '1.00', 0, 0, 'nooo', '2019-02-11 10:04:29', '2019-02-11 10:21:17'),
(24, 17, 24, 'close', '2019-02-11 15:51:17', '1.00', 0, 0, 'nooo', '2019-02-11 10:05:58', '2019-02-11 10:21:17'),
(25, 17, 24, 'close', '2019-02-11 15:51:17', '1.00', 0, 0, 'nooo', '2019-02-11 10:08:23', '2019-02-11 10:21:17'),
(26, 17, 24, 'open', NULL, '0.00', 0, 0, NULL, '2019-02-11 10:55:33', '2019-02-11 10:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `cash_register_transactions`
--

DROP TABLE IF EXISTS `cash_register_transactions`;
CREATE TABLE IF NOT EXISTS `cash_register_transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cash_register_id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `pay_method` enum('cash','card','cheque','bank_transfer','custom_pay_1','custom_pay_2','custom_pay_3','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('debit','credit') COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_type` enum('initial','sell','transfer','refund') COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cash_register_transactions_cash_register_id_foreign` (`cash_register_id`)
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cash_register_transactions`
--

INSERT INTO `cash_register_transactions` (`id`, `cash_register_id`, `amount`, `pay_method`, `type`, `transaction_type`, `transaction_id`, `created_at`, `updated_at`) VALUES
(1, 1, '1000.00', 'cash', 'credit', 'initial', NULL, '2018-11-26 12:50:35', '2018-11-26 12:50:35'),
(2, 2, '0.00', 'cash', 'credit', 'initial', NULL, '2018-11-28 05:18:27', '2018-11-28 05:18:27'),
(3, 3, '0.00', 'cash', 'credit', 'initial', NULL, '2018-11-28 05:19:09', '2018-11-28 05:19:09'),
(4, 4, '100.00', 'cash', 'credit', 'initial', NULL, '2018-11-28 08:01:46', '2018-11-28 08:01:46'),
(5, 5, '0.00', 'cash', 'credit', 'initial', NULL, '2018-11-28 09:42:14', '2018-11-28 09:42:14'),
(6, 6, '100.00', 'cash', 'credit', 'initial', NULL, '2018-11-30 11:30:15', '2018-11-30 11:30:15'),
(7, 6, '200.00', 'cash', 'credit', 'sell', 5, '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(8, 6, '0.00', 'cash', 'credit', 'sell', 5, '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(9, 7, '0.00', 'cash', 'credit', 'initial', NULL, '2018-12-13 13:06:09', '2018-12-13 13:06:09'),
(10, 8, '0.00', 'cash', 'credit', 'initial', NULL, '2018-12-29 07:48:10', '2018-12-29 07:48:10'),
(11, 9, '1000.00', 'cash', 'credit', 'initial', NULL, '2019-01-16 09:29:07', '2019-01-16 09:29:07'),
(12, 10, '1500.00', 'cash', 'credit', 'initial', NULL, '2019-01-16 10:08:32', '2019-01-16 10:08:32'),
(13, 11, '100.00', 'cash', 'credit', 'initial', NULL, '2019-02-08 05:38:11', '2019-02-08 05:38:11'),
(14, 11, '62500.00', 'cash', 'credit', 'sell', 13, '2019-02-08 06:19:54', '2019-02-08 06:19:54'),
(15, 11, '0.00', 'cash', 'credit', 'sell', 13, '2019-02-08 06:19:54', '2019-02-08 06:19:54'),
(16, 12, '10000.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:04:45', '2019-02-11 07:04:45'),
(17, 13, '1000.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:44:25', '2019-02-11 07:44:25'),
(18, 14, '111.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:45:53', '2019-02-11 07:45:53'),
(19, 15, '111.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:46:23', '2019-02-11 07:46:23'),
(20, 16, '111.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:46:29', '2019-02-11 07:46:29'),
(21, 17, '111.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:48:33', '2019-02-11 07:48:33'),
(22, 18, '10007.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 07:52:23', '2019-02-11 07:52:23'),
(23, 19, '2019.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 09:05:21', '2019-02-11 09:05:21'),
(24, 19, '62500.00', 'cash', 'credit', 'sell', 16, '2019-02-11 09:08:50', '2019-02-11 09:08:50'),
(25, 19, '0.00', 'cash', 'credit', 'sell', 16, '2019-02-11 09:08:50', '2019-02-11 09:08:50'),
(26, 20, '10000.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 09:44:01', '2019-02-11 09:44:01'),
(27, 21, '1000.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 09:45:09', '2019-02-11 09:45:09'),
(28, 22, '2019.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 10:04:27', '2019-02-11 10:04:27'),
(29, 23, '2019.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 10:04:29', '2019-02-11 10:04:29'),
(30, 24, '2019.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 10:05:58', '2019-02-11 10:05:58'),
(31, 25, '2019.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 10:08:23', '2019-02-11 10:08:23'),
(32, 26, '1000.00', 'cash', 'credit', 'initial', NULL, '2019-02-11 10:55:33', '2019-02-11 10:55:33'),
(33, 11, '250000.00', 'cash', 'credit', 'sell', 17, '2019-02-11 11:07:27', '2019-02-11 11:07:27'),
(34, 11, '0.00', 'cash', 'credit', 'sell', 17, '2019-02-11 11:07:27', '2019-02-11 11:07:27'),
(35, 11, '62500.00', 'cash', 'credit', 'sell', 18, '2019-02-12 06:00:29', '2019-02-12 06:00:29'),
(36, 11, '0.00', 'cash', 'credit', 'sell', 18, '2019-02-12 06:00:29', '2019-02-12 06:00:29'),
(37, 11, '62500.00', 'cash', 'credit', 'sell', 19, '2019-02-12 06:01:04', '2019-02-12 06:01:04'),
(38, 11, '0.00', 'cash', 'credit', 'sell', 19, '2019-02-12 06:01:04', '2019-02-12 06:01:04'),
(39, 26, '62500.00', 'cash', 'credit', 'sell', 20, '2019-02-12 06:22:42', '2019-02-12 06:22:42'),
(40, 26, '0.00', 'cash', 'credit', 'sell', 20, '2019-02-12 06:22:42', '2019-02-12 06:22:42'),
(41, 26, '62500.00', 'cash', 'credit', 'sell', 22, '2019-02-12 06:29:59', '2019-02-12 06:29:59'),
(42, 26, '0.00', 'cash', 'credit', 'sell', 22, '2019-02-12 06:29:59', '2019-02-12 06:29:59'),
(43, 26, '62500.00', 'cash', 'credit', 'sell', 27, '2019-02-12 08:55:38', '2019-02-12 08:55:38'),
(44, 26, '0.00', 'cash', 'credit', 'sell', 27, '2019-02-12 08:55:38', '2019-02-12 08:55:38'),
(45, 26, '62500.00', 'card', 'credit', 'sell', 28, '2019-02-12 08:57:22', '2019-02-12 08:57:22'),
(46, 26, '0.00', 'cash', 'credit', 'sell', 28, '2019-02-12 08:57:22', '2019-02-12 08:57:22'),
(47, 26, '62500.00', 'cash', 'credit', 'sell', 30, '2019-02-12 09:11:56', '2019-02-12 09:11:56'),
(48, 26, '0.00', 'cash', 'credit', 'sell', 30, '2019-02-12 09:11:56', '2019-02-12 09:11:56'),
(49, 26, '62500.00', 'cash', 'credit', 'sell', 31, '2019-02-12 09:13:48', '2019-02-12 09:13:48'),
(50, 26, '0.00', 'cash', 'credit', 'sell', 31, '2019-02-12 09:13:48', '2019-02-12 09:13:48'),
(51, 26, '0.00', 'cash', 'credit', 'sell', 60, '2019-02-13 11:35:40', '2019-02-13 11:35:40'),
(52, 26, '125010.00', 'cash', 'credit', 'sell', 60, '2019-02-13 11:35:40', '2019-02-13 11:35:40'),
(53, 26, '0.00', 'cash', 'credit', 'sell', 60, '2019-02-13 11:35:40', '2019-02-13 11:35:40'),
(54, 11, '288750.00', 'cash', 'credit', 'sell', 62, '2019-02-14 05:28:39', '2019-02-14 05:28:39'),
(55, 11, '0.00', 'cash', 'credit', 'sell', 62, '2019-02-14 05:28:39', '2019-02-14 05:28:39'),
(56, 11, '288750.00', 'cash', 'credit', 'sell', 63, '2019-02-14 05:30:06', '2019-02-14 05:30:06'),
(57, 11, '0.00', 'cash', 'credit', 'sell', 63, '2019-02-14 05:30:06', '2019-02-14 05:30:06'),
(62, 26, '288750.00', 'cash', 'credit', 'sell', 66, '2019-02-14 06:10:19', '2019-02-14 06:10:19'),
(63, 26, '0.00', 'cash', 'credit', 'sell', 66, '2019-02-14 06:10:19', '2019-02-14 06:10:19'),
(64, 26, '288750.00', 'cash', 'credit', 'sell', 69, '2019-02-14 06:14:49', '2019-02-14 06:14:49'),
(65, 26, '0.00', 'cash', 'credit', 'sell', 69, '2019-02-14 06:14:49', '2019-02-14 06:14:49'),
(88, 26, '288750.00', 'cash', 'credit', 'sell', 84, '2019-02-14 06:55:56', '2019-02-14 06:55:56'),
(89, 26, '0.00', 'cash', 'credit', 'sell', 84, '2019-02-14 06:55:56', '2019-02-14 06:55:56'),
(90, 26, '288750.00', 'cash', 'credit', 'sell', 85, '2019-02-14 06:57:43', '2019-02-14 06:57:43'),
(91, 26, '0.00', 'cash', 'credit', 'sell', 85, '2019-02-14 06:57:43', '2019-02-14 06:57:43'),
(92, 26, '288750.00', 'cash', 'credit', 'sell', 86, '2019-02-14 06:58:00', '2019-02-14 06:58:00'),
(93, 26, '0.00', 'cash', 'credit', 'sell', 86, '2019-02-14 06:58:00', '2019-02-14 06:58:00'),
(94, 26, '288750.00', 'cash', 'credit', 'sell', 87, '2019-02-14 06:58:13', '2019-02-14 06:58:13'),
(95, 26, '0.00', 'cash', 'credit', 'sell', 87, '2019-02-14 06:58:13', '2019-02-14 06:58:13'),
(96, 26, '288750.00', 'cash', 'credit', 'sell', 88, '2019-02-14 06:58:24', '2019-02-14 06:58:24'),
(97, 26, '0.00', 'cash', 'credit', 'sell', 88, '2019-02-14 06:58:24', '2019-02-14 06:58:24'),
(98, 26, '288750.00', 'cash', 'credit', 'sell', 89, '2019-02-14 06:58:38', '2019-02-14 06:58:38'),
(99, 26, '0.00', 'cash', 'credit', 'sell', 89, '2019-02-14 06:58:38', '2019-02-14 06:58:38'),
(100, 26, '288750.00', 'cash', 'credit', 'sell', 90, '2019-02-14 06:59:04', '2019-02-14 06:59:04'),
(101, 26, '0.00', 'cash', 'credit', 'sell', 90, '2019-02-14 06:59:04', '2019-02-14 06:59:04'),
(102, 26, '288750.00', 'cash', 'credit', 'sell', 91, '2019-02-14 07:04:24', '2019-02-14 07:04:24'),
(103, 26, '0.00', 'cash', 'credit', 'sell', 91, '2019-02-14 07:04:24', '2019-02-14 07:04:24'),
(104, 26, '288750.00', 'cash', 'credit', 'sell', 92, '2019-02-14 07:04:47', '2019-02-14 07:04:47'),
(105, 26, '0.00', 'cash', 'credit', 'sell', 92, '2019-02-14 07:04:47', '2019-02-14 07:04:47'),
(106, 26, '288750.00', 'cash', 'credit', 'sell', 93, '2019-02-14 07:04:57', '2019-02-14 07:04:57'),
(107, 26, '0.00', 'cash', 'credit', 'sell', 93, '2019-02-14 07:04:57', '2019-02-14 07:04:57'),
(108, 26, '96250.00', 'cash', 'credit', 'sell', 94, '2019-02-14 07:09:06', '2019-02-14 07:09:06'),
(109, 26, '0.00', 'cash', 'credit', 'sell', 94, '2019-02-14 07:09:06', '2019-02-14 07:09:06'),
(110, 26, '96250.00', 'cash', 'credit', 'sell', 95, '2019-02-14 07:09:35', '2019-02-14 07:09:35'),
(111, 26, '0.00', 'cash', 'credit', 'sell', 95, '2019-02-14 07:09:35', '2019-02-14 07:09:35'),
(112, 26, '96250.00', 'cash', 'credit', 'sell', 96, '2019-02-14 07:10:49', '2019-02-14 07:10:49'),
(113, 26, '0.00', 'cash', 'credit', 'sell', 96, '2019-02-14 07:10:49', '2019-02-14 07:10:49'),
(114, 26, '288750.00', 'cash', 'credit', 'sell', 97, '2019-02-14 07:15:11', '2019-02-14 07:15:11'),
(115, 26, '0.00', 'cash', 'credit', 'sell', 97, '2019-02-14 07:15:11', '2019-02-14 07:15:11'),
(116, 26, '288750.00', 'cash', 'credit', 'sell', 98, '2019-02-14 07:15:44', '2019-02-14 07:15:44'),
(117, 26, '0.00', 'cash', 'credit', 'sell', 98, '2019-02-14 07:15:44', '2019-02-14 07:15:44'),
(118, 26, '96250.00', 'cash', 'credit', 'sell', 99, '2019-02-14 07:16:16', '2019-02-14 07:16:16'),
(119, 26, '0.00', 'cash', 'credit', 'sell', 99, '2019-02-14 07:16:16', '2019-02-14 07:16:16'),
(120, 26, '288750.00', 'cash', 'credit', 'sell', 100, '2019-02-14 07:17:59', '2019-02-14 07:17:59'),
(121, 26, '0.00', 'cash', 'credit', 'sell', 100, '2019-02-14 07:17:59', '2019-02-14 07:17:59'),
(122, 26, '288750.00', 'cash', 'credit', 'sell', 101, '2019-02-14 07:19:35', '2019-02-14 07:19:35'),
(123, 26, '0.00', 'cash', 'credit', 'sell', 101, '2019-02-14 07:19:35', '2019-02-14 07:19:35'),
(124, 26, '288750.00', 'cash', 'credit', 'sell', 102, '2019-02-14 07:20:58', '2019-02-14 07:20:58'),
(125, 26, '0.00', 'cash', 'credit', 'sell', 102, '2019-02-14 07:20:58', '2019-02-14 07:20:58'),
(126, 26, '288750.00', 'cash', 'credit', 'sell', 103, '2019-02-14 07:39:14', '2019-02-14 07:39:14'),
(127, 26, '0.00', 'cash', 'credit', 'sell', 103, '2019-02-14 07:39:14', '2019-02-14 07:39:14'),
(128, 26, '288750.00', 'cash', 'credit', 'sell', 104, '2019-02-14 07:39:15', '2019-02-14 07:39:15'),
(129, 26, '0.00', 'cash', 'credit', 'sell', 104, '2019-02-14 07:39:15', '2019-02-14 07:39:15'),
(130, 26, '288750.00', 'cash', 'credit', 'sell', 105, '2019-02-14 07:42:24', '2019-02-14 07:42:24'),
(131, 26, '0.00', 'cash', 'credit', 'sell', 105, '2019-02-14 07:42:24', '2019-02-14 07:42:24'),
(132, 26, '288750.00', 'cash', 'credit', 'sell', 106, '2019-02-14 07:42:28', '2019-02-14 07:42:28'),
(133, 26, '0.00', 'cash', 'credit', 'sell', 106, '2019-02-14 07:42:28', '2019-02-14 07:42:28'),
(134, 26, '288750.00', 'cash', 'credit', 'sell', 107, '2019-02-14 07:42:45', '2019-02-14 07:42:45'),
(135, 26, '0.00', 'cash', 'credit', 'sell', 107, '2019-02-14 07:42:45', '2019-02-14 07:42:45'),
(136, 26, '288750.00', 'cash', 'credit', 'sell', 108, '2019-02-14 07:44:54', '2019-02-14 07:44:54'),
(137, 26, '0.00', 'cash', 'credit', 'sell', 108, '2019-02-14 07:44:54', '2019-02-14 07:44:54'),
(138, 26, '288750.00', 'cash', 'credit', 'sell', 109, '2019-02-14 07:47:37', '2019-02-14 07:47:37'),
(139, 26, '0.00', 'cash', 'credit', 'sell', 109, '2019-02-14 07:47:37', '2019-02-14 07:47:37'),
(140, 26, '288750.00', 'cash', 'credit', 'sell', 110, '2019-02-14 07:51:59', '2019-02-14 07:51:59'),
(141, 26, '0.00', 'cash', 'credit', 'sell', 110, '2019-02-14 07:51:59', '2019-02-14 07:51:59'),
(142, 26, '288750.00', 'cash', 'credit', 'sell', 111, '2019-02-14 07:52:29', '2019-02-14 07:52:29'),
(143, 26, '0.00', 'cash', 'credit', 'sell', 111, '2019-02-14 07:52:29', '2019-02-14 07:52:29'),
(144, 26, '288750.00', 'cash', 'credit', 'sell', 112, '2019-02-14 07:53:39', '2019-02-14 07:53:39'),
(145, 26, '0.00', 'cash', 'credit', 'sell', 112, '2019-02-14 07:53:39', '2019-02-14 07:53:39'),
(146, 26, '288750.00', 'cash', 'credit', 'sell', 113, '2019-02-14 07:53:56', '2019-02-14 07:53:56'),
(147, 26, '0.00', 'cash', 'credit', 'sell', 113, '2019-02-14 07:53:56', '2019-02-14 07:53:56'),
(148, 26, '288750.00', 'cash', 'credit', 'sell', 114, '2019-02-14 07:54:07', '2019-02-14 07:54:07'),
(149, 26, '0.00', 'cash', 'credit', 'sell', 114, '2019-02-14 07:54:07', '2019-02-14 07:54:07'),
(150, 26, '288750.00', 'cash', 'credit', 'sell', 115, '2019-02-14 07:54:28', '2019-02-14 07:54:28'),
(151, 26, '0.00', 'cash', 'credit', 'sell', 115, '2019-02-14 07:54:28', '2019-02-14 07:54:28'),
(152, 26, '288750.00', 'cash', 'credit', 'sell', 116, '2019-02-14 07:56:05', '2019-02-14 07:56:05'),
(153, 26, '0.00', 'cash', 'credit', 'sell', 116, '2019-02-14 07:56:05', '2019-02-14 07:56:05'),
(154, 26, '288750.00', 'cash', 'credit', 'sell', 117, '2019-02-14 07:56:27', '2019-02-14 07:56:27'),
(155, 26, '0.00', 'cash', 'credit', 'sell', 117, '2019-02-14 07:56:27', '2019-02-14 07:56:27'),
(156, 26, '288750.00', 'cash', 'credit', 'sell', 118, '2019-02-14 07:56:54', '2019-02-14 07:56:54'),
(157, 26, '0.00', 'cash', 'credit', 'sell', 118, '2019-02-14 07:56:54', '2019-02-14 07:56:54'),
(158, 26, '288750.00', 'cash', 'credit', 'sell', 119, '2019-02-14 07:57:33', '2019-02-14 07:57:33'),
(159, 26, '0.00', 'cash', 'credit', 'sell', 119, '2019-02-14 07:57:33', '2019-02-14 07:57:33'),
(160, 26, '288750.00', 'cash', 'credit', 'sell', 120, '2019-02-14 07:57:48', '2019-02-14 07:57:48'),
(161, 26, '0.00', 'cash', 'credit', 'sell', 120, '2019-02-14 07:57:48', '2019-02-14 07:57:48'),
(162, 26, '288750.00', 'cash', 'credit', 'sell', 121, '2019-02-14 07:58:19', '2019-02-14 07:58:19'),
(163, 26, '0.00', 'cash', 'credit', 'sell', 121, '2019-02-14 07:58:19', '2019-02-14 07:58:19'),
(164, 26, '288750.00', 'cash', 'credit', 'sell', 122, '2019-02-14 07:58:48', '2019-02-14 07:58:48'),
(165, 26, '0.00', 'cash', 'credit', 'sell', 122, '2019-02-14 07:58:48', '2019-02-14 07:58:48'),
(166, 26, '288750.00', 'cash', 'credit', 'sell', 123, '2019-02-14 07:59:41', '2019-02-14 07:59:41'),
(167, 26, '0.00', 'cash', 'credit', 'sell', 123, '2019-02-14 07:59:41', '2019-02-14 07:59:41'),
(168, 26, '288750.00', 'cash', 'credit', 'sell', 124, '2019-02-14 07:59:43', '2019-02-14 07:59:43'),
(169, 26, '0.00', 'cash', 'credit', 'sell', 124, '2019-02-14 07:59:43', '2019-02-14 07:59:43'),
(170, 26, '288750.00', 'cash', 'credit', 'sell', 125, '2019-02-14 07:59:57', '2019-02-14 07:59:57'),
(171, 26, '0.00', 'cash', 'credit', 'sell', 125, '2019-02-14 07:59:57', '2019-02-14 07:59:57'),
(172, 26, '288750.00', 'cash', 'credit', 'sell', 126, '2019-02-14 08:00:30', '2019-02-14 08:00:30'),
(173, 26, '0.00', 'cash', 'credit', 'sell', 126, '2019-02-14 08:00:30', '2019-02-14 08:00:30'),
(174, 26, '288750.00', 'cash', 'credit', 'sell', 127, '2019-02-14 08:00:56', '2019-02-14 08:00:56'),
(175, 26, '0.00', 'cash', 'credit', 'sell', 127, '2019-02-14 08:00:56', '2019-02-14 08:00:56'),
(176, 26, '288750.00', 'cash', 'credit', 'sell', 128, '2019-02-14 08:02:59', '2019-02-14 08:02:59'),
(177, 26, '0.00', 'cash', 'credit', 'sell', 128, '2019-02-14 08:02:59', '2019-02-14 08:02:59'),
(178, 26, '288750.00', 'cash', 'credit', 'sell', 129, '2019-02-14 08:03:27', '2019-02-14 08:03:27'),
(179, 26, '0.00', 'cash', 'credit', 'sell', 129, '2019-02-14 08:03:27', '2019-02-14 08:03:27'),
(180, 26, '288750.00', 'cash', 'credit', 'sell', 130, '2019-02-14 09:12:02', '2019-02-14 09:12:02'),
(181, 26, '0.00', 'cash', 'credit', 'sell', 130, '2019-02-14 09:12:02', '2019-02-14 09:12:02'),
(182, 26, '288750.00', 'cash', 'credit', 'sell', 131, '2019-02-14 09:15:02', '2019-02-14 09:15:02'),
(183, 26, '0.00', 'cash', 'credit', 'sell', 131, '2019-02-14 09:15:02', '2019-02-14 09:15:02'),
(190, 26, '62500.00', 'cash', 'credit', 'sell', 135, '2019-02-14 09:20:07', '2019-02-14 09:20:07'),
(191, 26, '0.00', 'cash', 'credit', 'sell', 135, '2019-02-14 09:20:07', '2019-02-14 09:20:07'),
(192, 26, '62500.00', 'cash', 'credit', 'sell', 137, '2019-02-14 09:22:59', '2019-02-14 09:22:59'),
(193, 26, '0.00', 'cash', 'credit', 'sell', 137, '2019-02-14 09:22:59', '2019-02-14 09:22:59'),
(194, 26, '62500.00', 'cash', 'credit', 'sell', 138, '2019-02-14 09:24:48', '2019-02-14 09:24:48'),
(195, 26, '0.00', 'cash', 'credit', 'sell', 138, '2019-02-14 09:24:48', '2019-02-14 09:24:48'),
(196, 26, '62500.00', 'cash', 'credit', 'sell', 139, '2019-02-14 09:25:12', '2019-02-14 09:25:12'),
(197, 26, '0.00', 'cash', 'credit', 'sell', 139, '2019-02-14 09:25:12', '2019-02-14 09:25:12'),
(198, 26, '62500.00', 'cash', 'credit', 'sell', 140, '2019-02-14 09:38:32', '2019-02-14 09:38:32'),
(199, 26, '0.00', 'cash', 'credit', 'sell', 140, '2019-02-14 09:38:32', '2019-02-14 09:38:32'),
(200, 26, '62500.00', 'cash', 'credit', 'sell', 141, '2019-02-14 09:38:36', '2019-02-14 09:38:36'),
(201, 26, '0.00', 'cash', 'credit', 'sell', 141, '2019-02-14 09:38:36', '2019-02-14 09:38:36'),
(202, 26, '62500.00', 'cash', 'credit', 'sell', 142, '2019-02-14 09:39:12', '2019-02-14 09:39:12'),
(203, 26, '0.00', 'cash', 'credit', 'sell', 142, '2019-02-14 09:39:12', '2019-02-14 09:39:12'),
(204, 26, '250000.00', 'cash', 'credit', 'sell', 143, '2019-02-14 09:42:38', '2019-02-14 09:42:38'),
(205, 26, '0.00', 'cash', 'credit', 'sell', 143, '2019-02-14 09:42:38', '2019-02-14 09:42:38'),
(206, 26, '250000.00', 'cash', 'credit', 'sell', 144, '2019-02-14 09:47:57', '2019-02-14 09:47:57'),
(207, 26, '0.00', 'cash', 'credit', 'sell', 144, '2019-02-14 09:47:57', '2019-02-14 09:47:57'),
(210, 26, '288750.00', 'cash', 'credit', 'sell', 146, '2019-02-14 10:05:55', '2019-02-14 10:05:55'),
(211, 26, '0.00', 'cash', 'credit', 'sell', 146, '2019-02-14 10:05:55', '2019-02-14 10:05:55'),
(212, 26, '288750.00', 'cash', 'credit', 'sell', 147, '2019-02-14 10:07:37', '2019-02-14 10:07:37'),
(213, 26, '0.00', 'cash', 'credit', 'sell', 147, '2019-02-14 10:07:37', '2019-02-14 10:07:37'),
(214, 26, '288750.00', 'cash', 'credit', 'sell', 148, '2019-02-14 10:11:18', '2019-02-14 10:11:18'),
(215, 26, '0.00', 'cash', 'credit', 'sell', 148, '2019-02-14 10:11:18', '2019-02-14 10:11:18'),
(216, 26, '96250.00', 'card', 'credit', 'sell', 149, '2019-02-14 10:12:12', '2019-02-14 10:12:12'),
(217, 26, '0.00', 'cash', 'credit', 'sell', 149, '2019-02-14 10:12:12', '2019-02-14 10:12:12'),
(218, 26, '288750.00', 'cash', 'credit', 'sell', 150, '2019-02-14 10:23:50', '2019-02-14 10:23:50'),
(219, 26, '0.00', 'cash', 'credit', 'sell', 150, '2019-02-14 10:23:50', '2019-02-14 10:23:50'),
(220, 26, '283750.00', 'cash', 'credit', 'sell', 152, '2019-02-14 10:28:22', '2019-02-14 10:28:22'),
(221, 26, '0.00', 'cash', 'credit', 'sell', 152, '2019-02-14 10:28:22', '2019-02-14 10:28:22'),
(222, 26, '283760.00', 'cash', 'credit', 'sell', 153, '2019-02-14 10:34:25', '2019-02-14 10:34:25'),
(223, 26, '0.00', 'cash', 'credit', 'sell', 153, '2019-02-14 10:34:25', '2019-02-14 10:34:25'),
(224, 26, '413760.00', 'cash', 'credit', 'sell', 154, '2019-02-14 10:48:28', '2019-02-14 10:48:28'),
(225, 26, '0.00', 'cash', 'credit', 'sell', 154, '2019-02-14 10:48:28', '2019-02-14 10:48:28'),
(226, 26, '413760.00', 'cash', 'credit', 'sell', 155, '2019-02-14 10:53:45', '2019-02-14 10:53:45'),
(227, 26, '0.00', 'cash', 'credit', 'sell', 155, '2019-02-14 10:53:45', '2019-02-14 10:53:45'),
(228, 26, '413760.00', 'cash', 'credit', 'sell', 156, '2019-02-14 10:53:49', '2019-02-14 10:53:49'),
(229, 26, '0.00', 'cash', 'credit', 'sell', 156, '2019-02-14 10:53:49', '2019-02-14 10:53:49'),
(230, 26, '413760.00', 'cash', 'credit', 'sell', 157, '2019-02-14 10:54:07', '2019-02-14 10:54:07'),
(231, 26, '0.00', 'cash', 'credit', 'sell', 157, '2019-02-14 10:54:07', '2019-02-14 10:54:07'),
(232, 26, '413760.00', 'cash', 'credit', 'sell', 158, '2019-02-14 10:55:20', '2019-02-14 10:55:20'),
(233, 26, '0.00', 'cash', 'credit', 'sell', 158, '2019-02-14 10:55:20', '2019-02-14 10:55:20'),
(234, 26, '413760.00', 'cash', 'credit', 'sell', 159, '2019-02-14 10:55:38', '2019-02-14 10:55:38'),
(235, 26, '0.00', 'cash', 'credit', 'sell', 159, '2019-02-14 10:55:38', '2019-02-14 10:55:38'),
(236, 26, '413760.00', 'cash', 'credit', 'sell', 160, '2019-02-14 10:55:53', '2019-02-14 10:55:53'),
(237, 26, '0.00', 'cash', 'credit', 'sell', 160, '2019-02-14 10:55:53', '2019-02-14 10:55:53'),
(238, 26, '413760.00', 'cash', 'credit', 'sell', 161, '2019-02-14 10:57:45', '2019-02-14 10:57:45'),
(239, 26, '0.00', 'cash', 'credit', 'sell', 161, '2019-02-14 10:57:45', '2019-02-14 10:57:45'),
(240, 26, '413760.00', 'cash', 'credit', 'sell', 162, '2019-02-14 10:58:04', '2019-02-14 10:58:04'),
(241, 26, '0.00', 'cash', 'credit', 'sell', 162, '2019-02-14 10:58:04', '2019-02-14 10:58:04'),
(242, 26, '413760.00', 'cash', 'credit', 'sell', 163, '2019-02-14 10:58:22', '2019-02-14 10:58:22'),
(243, 26, '0.00', 'cash', 'credit', 'sell', 163, '2019-02-14 10:58:22', '2019-02-14 10:58:22'),
(244, 26, '413760.00', 'cash', 'credit', 'sell', 164, '2019-02-14 10:58:35', '2019-02-14 10:58:35'),
(245, 26, '0.00', 'cash', 'credit', 'sell', 164, '2019-02-14 10:58:35', '2019-02-14 10:58:35'),
(246, 26, '413760.00', 'cash', 'credit', 'sell', 165, '2019-02-14 11:18:24', '2019-02-14 11:18:24'),
(247, 26, '0.00', 'cash', 'credit', 'sell', 165, '2019-02-14 11:18:24', '2019-02-14 11:18:24'),
(248, 26, '413760.00', 'cash', 'credit', 'sell', 166, '2019-02-14 11:43:34', '2019-02-14 11:43:34'),
(249, 26, '0.00', 'cash', 'credit', 'sell', 166, '2019-02-14 11:43:34', '2019-02-14 11:43:34'),
(250, 26, '413760.00', 'cash', 'credit', 'sell', 167, '2019-02-14 11:45:40', '2019-02-14 11:45:40'),
(251, 26, '0.00', 'cash', 'credit', 'sell', 167, '2019-02-14 11:45:40', '2019-02-14 11:45:40'),
(252, 26, '413760.00', 'cash', 'credit', 'sell', 168, '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(253, 26, '0.00', 'cash', 'credit', 'sell', 168, '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(254, 26, '413760.00', 'cash', 'credit', 'sell', 169, '2019-02-14 11:47:20', '2019-02-14 11:47:20'),
(255, 26, '0.00', 'cash', 'credit', 'sell', 169, '2019-02-14 11:47:20', '2019-02-14 11:47:20'),
(256, 26, '413760.00', 'cash', 'credit', 'sell', 170, '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(257, 26, '0.00', 'cash', 'credit', 'sell', 170, '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(258, 26, '413760.00', 'cash', 'credit', 'sell', 171, '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(259, 26, '0.00', 'cash', 'credit', 'sell', 171, '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(260, 26, '413760.00', 'cash', 'credit', 'sell', 172, '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(261, 26, '0.00', 'cash', 'credit', 'sell', 172, '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(262, 26, '413760.00', 'cash', 'credit', 'sell', 173, '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(263, 26, '0.00', 'cash', 'credit', 'sell', 173, '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(264, 26, '413760.00', 'cash', 'credit', 'sell', 174, '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(265, 26, '0.00', 'cash', 'credit', 'sell', 174, '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(266, 26, '96250.00', 'cash', 'credit', 'sell', 177, '2019-02-15 05:30:40', '2019-02-15 05:30:40'),
(267, 26, '0.00', 'cash', 'credit', 'sell', 177, '2019-02-15 05:30:40', '2019-02-15 05:30:40'),
(268, 26, '413760.00', 'cash', 'credit', 'sell', 178, '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(269, 26, '0.00', 'cash', 'credit', 'sell', 178, '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(270, 26, '413760.00', 'cash', 'credit', 'sell', 179, '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(271, 26, '0.00', 'cash', 'credit', 'sell', 179, '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(278, 26, '413760.00', 'cash', 'credit', 'sell', 183, '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(279, 26, '0.00', 'cash', 'credit', 'sell', 183, '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(280, 26, '413760.00', 'cash', 'credit', 'sell', 184, '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(281, 26, '0.00', 'cash', 'credit', 'sell', 184, '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(282, 26, '413760.00', 'cash', 'credit', 'sell', 185, '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(283, 26, '0.00', 'cash', 'credit', 'sell', 185, '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(284, 26, '96250.00', 'cash', 'credit', 'sell', 186, '2019-02-15 06:23:12', '2019-02-15 06:23:12'),
(285, 26, '0.00', 'cash', 'credit', 'sell', 186, '2019-02-15 06:23:12', '2019-02-15 06:23:12'),
(286, 26, '413760.00', 'cash', 'credit', 'sell', 187, '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(287, 26, '0.00', 'cash', 'credit', 'sell', 187, '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(288, 26, '413760.00', 'cash', 'credit', 'sell', 188, '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(289, 26, '0.00', 'cash', 'credit', 'sell', 188, '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(290, 26, '413760.00', 'cash', 'credit', 'sell', 189, '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(291, 26, '0.00', 'cash', 'credit', 'sell', 189, '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(292, 26, '413760.00', 'cash', 'credit', 'sell', 190, '2019-02-15 06:43:39', '2019-02-15 06:43:39'),
(293, 26, '0.00', 'cash', 'credit', 'sell', 190, '2019-02-15 06:43:39', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `short_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_business_id_foreign` (`business_id`),
  KEY `categories_created_by_foreign` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `business_id`, `short_code`, `parent_id`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Set top box', 3, NULL, 0, 3, NULL, '2018-11-28 06:14:14', '2018-11-28 06:14:14'),
(2, 'Men', 16, '<b>MEN001</b>', 4, 19, NULL, '2019-01-16 11:35:21', '2019-01-16 12:15:56'),
(3, 'Women', 16, '<b>WOMEN001</b>', 4, 19, NULL, '2019-01-16 11:57:10', '2019-01-16 12:16:03'),
(4, 'Clothing', 16, NULL, 0, 19, NULL, '2019-01-16 11:58:12', '2019-01-16 12:08:16'),
(5, 'Footwear', 16, NULL, 0, 19, NULL, '2019-01-16 12:16:49', '2019-01-16 12:16:49'),
(6, 'Men', 16, NULL, 5, 19, NULL, '2019-01-16 12:18:03', '2019-01-16 12:18:03'),
(7, 'Mobile', 17, NULL, 0, 22, NULL, '2019-02-08 05:56:10', '2019-02-08 05:56:10'),
(8, 'pc', 17, NULL, 0, 22, NULL, '2019-02-08 05:56:27', '2019-02-08 05:56:27'),
(9, 'Laptop', 17, NULL, 0, 22, NULL, '2019-02-08 05:56:37', '2019-02-08 05:56:37'),
(10, 'Smart phone', 17, NULL, 7, 22, NULL, '2019-02-08 05:58:34', '2019-02-08 05:58:34'),
(11, 'standard phone', 17, NULL, 7, 22, NULL, '2019-02-08 06:00:20', '2019-02-08 06:00:20'),
(12, 'Touch screen', 17, NULL, 9, 22, NULL, '2019-02-08 06:01:03', '2019-02-08 06:01:03'),
(13, 'Gaming laptop', 17, NULL, 9, 22, NULL, '2019-02-08 06:01:28', '2019-02-08 06:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `type` enum('supplier','customer','both') COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_business_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_term_number` int(11) DEFAULT NULL,
  `pay_term_type` enum('days','months') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_limit` decimal(20,2) DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) DEFAULT NULL,
  `custom_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacts_business_id_foreign` (`business_id`),
  KEY `contacts_created_by_foreign` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `business_id`, `type`, `supplier_business_name`, `name`, `email`, `contact_id`, `tax_number`, `city`, `state`, `country`, `landmark`, `mobile`, `landline`, `alternate_number`, `pay_term_number`, `pay_term_type`, `credit_limit`, `created_by`, `is_default`, `customer_group_id`, `custom_field1`, `custom_field2`, `custom_field3`, `custom_field4`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(2, 2, 'customer', NULL, 'kkWalk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(3, 3, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(4, 3, 'supplier', 'abc corp', 'abc', NULL, 'CO0002', NULL, NULL, NULL, NULL, NULL, '1111', NULL, NULL, 30, 'days', NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-28 06:19:19', '2018-11-28 06:19:19'),
(5, 4, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(6, 4, 'customer', NULL, 'sijo', NULL, 'CO0002', NULL, NULL, NULL, NULL, NULL, '35686164643', NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-30 11:32:16', '2018-11-30 11:32:16'),
(8, 6, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(9, 7, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 9, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(11, 9, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(12, 10, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(13, 10, 'supplier', 'RANTA TOURISM', 'RANTA', NULL, 'CO0002', NULL, NULL, NULL, NULL, NULL, '9895020016', NULL, NULL, NULL, 'days', NULL, 12, 0, NULL, NULL, NULL, NULL, NULL, '2018-12-13 12:45:29', '2018-12-12 12:04:04', '2018-12-13 12:45:29'),
(14, 10, 'supplier', 'RANTA TOURISM', 'RANTA', NULL, 'CO0003', NULL, NULL, NULL, NULL, NULL, '9895020016', NULL, NULL, NULL, 'days', NULL, 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-12 12:12:46', '2018-12-12 12:12:46'),
(15, 10, 'customer', NULL, 'PIYAR ALI', NULL, 'CO0004', NULL, NULL, NULL, NULL, NULL, '9895', NULL, NULL, NULL, 'days', NULL, 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-12 12:15:03', '2018-12-12 12:15:03'),
(16, 11, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 13, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(17, 12, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 14, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(18, 13, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(19, 14, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 16, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 12:10:20', '2018-12-28 12:10:20'),
(20, 15, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 17, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(21, 16, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 19, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(22, 16, 'customer', NULL, 'cus1', 'cus1@gmail.com', '8129057105', '2', 'kozhikode', 'kerala', 'india', 'ulcp', '8129057105', NULL, NULL, NULL, 'months', '100.00', 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-16 09:34:05', '2019-01-16 09:34:05'),
(23, 16, 'supplier', 'suplier1 businessnm', 'suplier1', 'suplier1@gmail.com', '9656557105', NULL, 'kozhikode', 'kerala', 'india', 'ulcp', '9656557105', NULL, NULL, NULL, 'months', NULL, 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-16 09:46:43', '2019-01-16 09:46:43'),
(24, 17, 'customer', NULL, 'Walk-In Customer', NULL, 'CO0001', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 22, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(25, 17, 'customer', NULL, 'customer1', 'customer1@gmail.com', '965655', '01100', 'Tirur', 'kerela', 'india', 'bus stand', '8129057105', NULL, NULL, NULL, 'days', '20000.00', 22, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-08 06:15:26', '2019-02-08 06:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thousand_separator` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_separator` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `country`, `currency`, `code`, `symbol`, `thousand_separator`, `decimal_separator`, `created_at`, `updated_at`) VALUES
(1, 'Albania', 'Leke', 'ALL', 'Lek', ',', '.', NULL, NULL),
(2, 'America', 'Dollars', 'USD', '$', ',', '.', NULL, NULL),
(3, 'Afghanistan', 'Afghanis', 'AF', '؋', ',', '.', NULL, NULL),
(4, 'Argentina', 'Pesos', 'ARS', '$', ',', '.', NULL, NULL),
(5, 'Aruba', 'Guilders', 'AWG', 'ƒ', ',', '.', NULL, NULL),
(6, 'Australia', 'Dollars', 'AUD', '$', ',', '.', NULL, NULL),
(7, 'Azerbaijan', 'New Manats', 'AZ', 'ман', ',', '.', NULL, NULL),
(8, 'Bahamas', 'Dollars', 'BSD', '$', ',', '.', NULL, NULL),
(9, 'Barbados', 'Dollars', 'BBD', '$', ',', '.', NULL, NULL),
(10, 'Belarus', 'Rubles', 'BYR', 'p.', ',', '.', NULL, NULL),
(11, 'Belgium', 'Euro', 'EUR', '€', ',', '.', NULL, NULL),
(12, 'Beliz', 'Dollars', 'BZD', 'BZ$', ',', '.', NULL, NULL),
(13, 'Bermuda', 'Dollars', 'BMD', '$', ',', '.', NULL, NULL),
(14, 'Bolivia', 'Bolivianos', 'BOB', '$b', ',', '.', NULL, NULL),
(15, 'Bosnia and Herzegovina', 'Convertible Marka', 'BAM', 'KM', ',', '.', NULL, NULL),
(16, 'Botswana', 'Pula\'s', 'BWP', 'P', ',', '.', NULL, NULL),
(17, 'Bulgaria', 'Leva', 'BG', 'лв', ',', '.', NULL, NULL),
(18, 'Brazil', 'Reais', 'BRL', 'R$', ',', '.', NULL, NULL),
(19, 'Britain [United Kingdom]', 'Pounds', 'GBP', '£', ',', '.', NULL, NULL),
(20, 'Brunei Darussalam', 'Dollars', 'BND', '$', ',', '.', NULL, NULL),
(21, 'Cambodia', 'Riels', 'KHR', '៛', ',', '.', NULL, NULL),
(22, 'Canada', 'Dollars', 'CAD', '$', ',', '.', NULL, NULL),
(23, 'Cayman Islands', 'Dollars', 'KYD', '$', ',', '.', NULL, NULL),
(24, 'Chile', 'Pesos', 'CLP', '$', ',', '.', NULL, NULL),
(25, 'China', 'Yuan Renminbi', 'CNY', '¥', ',', '.', NULL, NULL),
(26, 'Colombia', 'Pesos', 'COP', '$', ',', '.', NULL, NULL),
(27, 'Costa Rica', 'Colón', 'CRC', '₡', ',', '.', NULL, NULL),
(28, 'Croatia', 'Kuna', 'HRK', 'kn', ',', '.', NULL, NULL),
(29, 'Cuba', 'Pesos', 'CUP', '₱', ',', '.', NULL, NULL),
(30, 'Cyprus', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(31, 'Czech Republic', 'Koruny', 'CZK', 'Kč', ',', '.', NULL, NULL),
(32, 'Denmark', 'Kroner', 'DKK', 'kr', ',', '.', NULL, NULL),
(33, 'Dominican Republic', 'Pesos', 'DOP ', 'RD$', ',', '.', NULL, NULL),
(34, 'East Caribbean', 'Dollars', 'XCD', '$', ',', '.', NULL, NULL),
(35, 'Egypt', 'Pounds', 'EGP', '£', ',', '.', NULL, NULL),
(36, 'El Salvador', 'Colones', 'SVC', '$', ',', '.', NULL, NULL),
(37, 'England [United Kingdom]', 'Pounds', 'GBP', '£', ',', '.', NULL, NULL),
(38, 'Euro', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(39, 'Falkland Islands', 'Pounds', 'FKP', '£', ',', '.', NULL, NULL),
(40, 'Fiji', 'Dollars', 'FJD', '$', ',', '.', NULL, NULL),
(41, 'France', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(42, 'Ghana', 'Cedis', 'GHC', '¢', ',', '.', NULL, NULL),
(43, 'Gibraltar', 'Pounds', 'GIP', '£', ',', '.', NULL, NULL),
(44, 'Greece', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(45, 'Guatemala', 'Quetzales', 'GTQ', 'Q', ',', '.', NULL, NULL),
(46, 'Guernsey', 'Pounds', 'GGP', '£', ',', '.', NULL, NULL),
(47, 'Guyana', 'Dollars', 'GYD', '$', ',', '.', NULL, NULL),
(48, 'Holland [Netherlands]', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(49, 'Honduras', 'Lempiras', 'HNL', 'L', ',', '.', NULL, NULL),
(50, 'Hong Kong', 'Dollars', 'HKD', '$', ',', '.', NULL, NULL),
(51, 'Hungary', 'Forint', 'HUF', 'Ft', ',', '.', NULL, NULL),
(52, 'Iceland', 'Kronur', 'ISK', 'kr', ',', '.', NULL, NULL),
(53, 'India', 'Rupees', 'INR', '₹', ',', '.', NULL, NULL),
(54, 'Indonesia', 'Rupiahs', 'IDR', 'Rp', ',', '.', NULL, NULL),
(55, 'Iran', 'Rials', 'IRR', '﷼', ',', '.', NULL, NULL),
(56, 'Ireland', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(57, 'Isle of Man', 'Pounds', 'IMP', '£', ',', '.', NULL, NULL),
(58, 'Israel', 'New Shekels', 'ILS', '₪', ',', '.', NULL, NULL),
(59, 'Italy', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(60, 'Jamaica', 'Dollars', 'JMD', 'J$', ',', '.', NULL, NULL),
(61, 'Japan', 'Yen', 'JPY', '¥', ',', '.', NULL, NULL),
(62, 'Jersey', 'Pounds', 'JEP', '£', ',', '.', NULL, NULL),
(63, 'Kazakhstan', 'Tenge', 'KZT', 'лв', ',', '.', NULL, NULL),
(64, 'Korea [North]', 'Won', 'KPW', '₩', ',', '.', NULL, NULL),
(65, 'Korea [South]', 'Won', 'KRW', '₩', ',', '.', NULL, NULL),
(66, 'Kyrgyzstan', 'Soms', 'KGS', 'лв', ',', '.', NULL, NULL),
(67, 'Laos', 'Kips', 'LAK', '₭', ',', '.', NULL, NULL),
(68, 'Latvia', 'Lati', 'LVL', 'Ls', ',', '.', NULL, NULL),
(69, 'Lebanon', 'Pounds', 'LBP', '£', ',', '.', NULL, NULL),
(70, 'Liberia', 'Dollars', 'LRD', '$', ',', '.', NULL, NULL),
(71, 'Liechtenstein', 'Switzerland Francs', 'CHF', 'CHF', ',', '.', NULL, NULL),
(72, 'Lithuania', 'Litai', 'LTL', 'Lt', ',', '.', NULL, NULL),
(73, 'Luxembourg', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(74, 'Macedonia', 'Denars', 'MKD', 'ден', ',', '.', NULL, NULL),
(75, 'Malaysia', 'Ringgits', 'MYR', 'RM', ',', '.', NULL, NULL),
(76, 'Malta', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(77, 'Mauritius', 'Rupees', 'MUR', '₨', ',', '.', NULL, NULL),
(78, 'Mexico', 'Pesos', 'MX', '$', ',', '.', NULL, NULL),
(79, 'Mongolia', 'Tugriks', 'MNT', '₮', ',', '.', NULL, NULL),
(80, 'Mozambique', 'Meticais', 'MZ', 'MT', ',', '.', NULL, NULL),
(81, 'Namibia', 'Dollars', 'NAD', '$', ',', '.', NULL, NULL),
(82, 'Nepal', 'Rupees', 'NPR', '₨', ',', '.', NULL, NULL),
(83, 'Netherlands Antilles', 'Guilders', 'ANG', 'ƒ', ',', '.', NULL, NULL),
(84, 'Netherlands', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(85, 'New Zealand', 'Dollars', 'NZD', '$', ',', '.', NULL, NULL),
(86, 'Nicaragua', 'Cordobas', 'NIO', 'C$', ',', '.', NULL, NULL),
(87, 'Nigeria', 'Nairas', 'NG', '₦', ',', '.', NULL, NULL),
(88, 'North Korea', 'Won', 'KPW', '₩', ',', '.', NULL, NULL),
(89, 'Norway', 'Krone', 'NOK', 'kr', ',', '.', NULL, NULL),
(90, 'Oman', 'Rials', 'OMR', '﷼', ',', '.', NULL, NULL),
(91, 'Pakistan', 'Rupees', 'PKR', '₨', ',', '.', NULL, NULL),
(92, 'Panama', 'Balboa', 'PAB', 'B/.', ',', '.', NULL, NULL),
(93, 'Paraguay', 'Guarani', 'PYG', 'Gs', ',', '.', NULL, NULL),
(94, 'Peru', 'Nuevos Soles', 'PE', 'S/.', ',', '.', NULL, NULL),
(95, 'Philippines', 'Pesos', 'PHP', 'Php', ',', '.', NULL, NULL),
(96, 'Poland', 'Zlotych', 'PL', 'zł', ',', '.', NULL, NULL),
(97, 'Qatar', 'Rials', 'QAR', '﷼', ',', '.', NULL, NULL),
(98, 'Romania', 'New Lei', 'RO', 'lei', ',', '.', NULL, NULL),
(99, 'Russia', 'Rubles', 'RUB', 'руб', ',', '.', NULL, NULL),
(100, 'Saint Helena', 'Pounds', 'SHP', '£', ',', '.', NULL, NULL),
(101, 'Saudi Arabia', 'Riyals', 'SAR', '﷼', ',', '.', NULL, NULL),
(102, 'Serbia', 'Dinars', 'RSD', 'Дин.', ',', '.', NULL, NULL),
(103, 'Seychelles', 'Rupees', 'SCR', '₨', ',', '.', NULL, NULL),
(104, 'Singapore', 'Dollars', 'SGD', '$', ',', '.', NULL, NULL),
(105, 'Slovenia', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(106, 'Solomon Islands', 'Dollars', 'SBD', '$', ',', '.', NULL, NULL),
(107, 'Somalia', 'Shillings', 'SOS', 'S', ',', '.', NULL, NULL),
(108, 'South Africa', 'Rand', 'ZAR', 'R', ',', '.', NULL, NULL),
(109, 'South Korea', 'Won', 'KRW', '₩', ',', '.', NULL, NULL),
(110, 'Spain', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(111, 'Sri Lanka', 'Rupees', 'LKR', '₨', ',', '.', NULL, NULL),
(112, 'Sweden', 'Kronor', 'SEK', 'kr', ',', '.', NULL, NULL),
(113, 'Switzerland', 'Francs', 'CHF', 'CHF', ',', '.', NULL, NULL),
(114, 'Suriname', 'Dollars', 'SRD', '$', ',', '.', NULL, NULL),
(115, 'Syria', 'Pounds', 'SYP', '£', ',', '.', NULL, NULL),
(116, 'Taiwan', 'New Dollars', 'TWD', 'NT$', ',', '.', NULL, NULL),
(117, 'Thailand', 'Baht', 'THB', '฿', ',', '.', NULL, NULL),
(118, 'Trinidad and Tobago', 'Dollars', 'TTD', 'TT$', ',', '.', NULL, NULL),
(119, 'Turkey', 'Lira', 'TRY', 'TL', ',', '.', NULL, NULL),
(120, 'Turkey', 'Liras', 'TRL', '£', ',', '.', NULL, NULL),
(121, 'Tuvalu', 'Dollars', 'TVD', '$', ',', '.', NULL, NULL),
(122, 'Ukraine', 'Hryvnia', 'UAH', '₴', ',', '.', NULL, NULL),
(123, 'United Kingdom', 'Pounds', 'GBP', '£', ',', '.', NULL, NULL),
(124, 'United States of America', 'Dollars', 'USD', '$', ',', '.', NULL, NULL),
(125, 'Uruguay', 'Pesos', 'UYU', '$U', ',', '.', NULL, NULL),
(126, 'Uzbekistan', 'Sums', 'UZS', 'лв', ',', '.', NULL, NULL),
(127, 'Vatican City', 'Euro', 'EUR', '€', '.', ',', NULL, NULL),
(128, 'Venezuela', 'Bolivares Fuertes', 'VEF', 'Bs', ',', '.', NULL, NULL),
(129, 'Vietnam', 'Dong', 'VND', '₫', ',', '.', NULL, NULL),
(130, 'Yemen', 'Rials', 'YER', '﷼', ',', '.', NULL, NULL),
(131, 'Zimbabwe', 'Zimbabwe Dollars', 'ZWD', 'Z$', ',', '.', NULL, NULL),
(132, 'Iraq', 'Iraqi dinar', 'IQD', 'د.ع', ',', '.', NULL, NULL),
(133, 'Kenya', 'Kenyan shilling', 'KES', 'KSh', ',', '.', NULL, NULL),
(134, 'Bangladesh', 'Taka', 'BDT', '৳', ',', '.', NULL, NULL),
(135, 'Algerie', 'Algerian dinar', 'DZD', 'د.ج', ' ', '.', NULL, NULL),
(136, 'United Arab Emirates', 'United Arab Emirates dirham', 'AED', 'د.إ', ',', '.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

DROP TABLE IF EXISTS `customer_groups`;
CREATE TABLE IF NOT EXISTS `customer_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(5,2) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_groups_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_groups`
--

INSERT INTO `customer_groups` (`id`, `business_id`, `name`, `amount`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 16, 'cus group1', 10.00, 20, '2019-01-16 09:43:16', '2019-01-16 09:43:16'),
(2, 16, 'cus group2', 50.00, 20, '2019-01-16 09:43:39', '2019-01-16 09:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `expense_categories`
--

DROP TABLE IF EXISTS `expense_categories`;
CREATE TABLE IF NOT EXISTS `expense_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expense_categories_business_id_foreign` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_sub_taxes`
--

DROP TABLE IF EXISTS `group_sub_taxes`;
CREATE TABLE IF NOT EXISTS `group_sub_taxes` (
  `group_tax_id` int(10) UNSIGNED NOT NULL,
  `tax_id` int(10) UNSIGNED NOT NULL,
  KEY `group_sub_taxes_group_tax_id_foreign` (`group_tax_id`),
  KEY `group_sub_taxes_tax_id_foreign` (`tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_layouts`
--

DROP TABLE IF EXISTS `invoice_layouts`;
CREATE TABLE IF NOT EXISTS `invoice_layouts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_text` text COLLATE utf8mb4_unicode_ci,
  `invoice_no_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quotation_no_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading_line1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading_line2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading_line3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading_line4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading_line5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_heading_not_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_heading_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quotation_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_total_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_due_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_client_id` tinyint(1) NOT NULL DEFAULT '0',
  `client_id_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_tax_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_time` tinyint(1) NOT NULL DEFAULT '1',
  `show_brand` tinyint(1) NOT NULL DEFAULT '0',
  `show_sku` tinyint(1) NOT NULL DEFAULT '1',
  `show_cat_code` tinyint(1) NOT NULL DEFAULT '1',
  `show_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `show_lot` tinyint(1) NOT NULL DEFAULT '0',
  `show_sale_description` tinyint(1) NOT NULL DEFAULT '0',
  `sales_person_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_sales_person` tinyint(1) NOT NULL DEFAULT '0',
  `table_product_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_qty_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_unit_price_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_subtotal_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_code_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_logo` tinyint(1) NOT NULL DEFAULT '0',
  `show_business_name` tinyint(1) NOT NULL DEFAULT '0',
  `show_location_name` tinyint(1) NOT NULL DEFAULT '1',
  `show_landmark` tinyint(1) NOT NULL DEFAULT '1',
  `show_city` tinyint(1) NOT NULL DEFAULT '1',
  `show_state` tinyint(1) NOT NULL DEFAULT '1',
  `show_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `show_country` tinyint(1) NOT NULL DEFAULT '1',
  `show_mobile_number` tinyint(1) NOT NULL DEFAULT '1',
  `show_alternate_number` tinyint(1) NOT NULL DEFAULT '0',
  `show_email` tinyint(1) NOT NULL DEFAULT '0',
  `show_tax_1` tinyint(1) NOT NULL DEFAULT '1',
  `show_tax_2` tinyint(1) NOT NULL DEFAULT '0',
  `show_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `show_payments` tinyint(1) NOT NULL DEFAULT '0',
  `show_customer` tinyint(1) NOT NULL DEFAULT '0',
  `customer_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `highlight_color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_text` text COLLATE utf8mb4_unicode_ci,
  `module_info` text COLLATE utf8mb4_unicode_ci,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `business_id` int(10) UNSIGNED NOT NULL,
  `design` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT 'classic',
  `cn_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'cn = credit note',
  `cn_no_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_amount_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_tax_headings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_layouts_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_layouts`
--

INSERT INTO `invoice_layouts` (`id`, `name`, `header_text`, `invoice_no_prefix`, `quotation_no_prefix`, `invoice_heading`, `sub_heading_line1`, `sub_heading_line2`, `sub_heading_line3`, `sub_heading_line4`, `sub_heading_line5`, `invoice_heading_not_paid`, `invoice_heading_paid`, `quotation_heading`, `sub_total_label`, `discount_label`, `tax_label`, `total_label`, `total_due_label`, `paid_label`, `show_client_id`, `client_id_label`, `client_tax_label`, `date_label`, `show_time`, `show_brand`, `show_sku`, `show_cat_code`, `show_expiry`, `show_lot`, `show_sale_description`, `sales_person_label`, `show_sales_person`, `table_product_label`, `table_qty_label`, `table_unit_price_label`, `table_subtotal_label`, `cat_code_label`, `logo`, `show_logo`, `show_business_name`, `show_location_name`, `show_landmark`, `show_city`, `show_state`, `show_zip_code`, `show_country`, `show_mobile_number`, `show_alternate_number`, `show_email`, `show_tax_1`, `show_tax_2`, `show_barcode`, `show_payments`, `show_customer`, `customer_label`, `highlight_color`, `footer_text`, `module_info`, `is_default`, `business_id`, `design`, `cn_heading`, `cn_no_label`, `cn_amount_label`, `table_tax_headings`, `created_at`, `updated_at`) VALUES
(1, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 1, 'classic', NULL, NULL, NULL, NULL, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(2, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 2, 'classic', NULL, NULL, NULL, NULL, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(3, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 3, 'classic', NULL, NULL, NULL, NULL, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(4, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 4, 'classic', NULL, NULL, NULL, NULL, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(6, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 6, 'classic', NULL, NULL, NULL, NULL, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(7, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 7, 'classic', NULL, NULL, NULL, NULL, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(9, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 9, 'classic', NULL, NULL, NULL, NULL, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(10, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 10, 'classic', NULL, NULL, NULL, NULL, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(11, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 11, 'classic', NULL, NULL, NULL, NULL, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(12, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 12, 'classic', NULL, NULL, NULL, NULL, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(13, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 13, 'classic', NULL, NULL, NULL, NULL, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(14, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 14, 'classic', NULL, NULL, NULL, NULL, '2018-12-28 12:10:20', '2018-12-28 12:10:20'),
(15, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 15, 'classic', NULL, NULL, NULL, NULL, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(16, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 16, 'classic', NULL, NULL, NULL, NULL, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(17, 'Default', NULL, 'Invoice No.', NULL, 'Invoice', NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Subtotal', 'Discount', 'Tax', 'Total', 'Total Due', 'Total Paid', 0, NULL, NULL, 'Date', 1, 0, 1, 1, 0, 0, 0, NULL, 0, 'Product', 'Quantity', 'Unit Price', 'Subtotal', NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 'Customer', '#000000', '', NULL, 1, 17, 'classic', NULL, NULL, NULL, NULL, '2019-02-08 05:33:52', '2019-02-08 05:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_schemes`
--

DROP TABLE IF EXISTS `invoice_schemes`;
CREATE TABLE IF NOT EXISTS `invoice_schemes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scheme_type` enum('blank','year') COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_number` int(11) DEFAULT NULL,
  `invoice_count` int(11) NOT NULL DEFAULT '0',
  `total_digits` int(11) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_schemes_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_schemes`
--

INSERT INTO `invoice_schemes` (`id`, `business_id`, `name`, `scheme_type`, `prefix`, `start_number`, `invoice_count`, `total_digits`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 1, 'Default', 'blank', '', 1, 0, 4, 1, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(2, 2, 'Default', 'blank', '', 1, 0, 4, 1, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(3, 3, 'Default', 'blank', '', 1, 0, 4, 1, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(4, 4, 'Default', 'blank', '', 1, 1, 4, 1, '2018-11-28 07:25:40', '2018-11-30 11:39:32'),
(6, 6, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(7, 7, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(9, 9, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(10, 10, 'Default', 'blank', '', 1, 1, 4, 1, '2018-12-12 13:09:05', '2018-12-12 12:15:49'),
(11, 11, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(12, 12, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(13, 13, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(14, 14, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-28 12:10:20', '2018-12-28 12:10:20'),
(15, 15, 'Default', 'blank', '', 1, 0, 4, 1, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(16, 16, 'Default', 'blank', '', 1, 0, 4, 1, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(17, 17, 'Default', 'blank', '', 1, 112, 4, 1, '2019-02-08 05:33:52', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_07_05_071953_create_currencies_table', 1),
(4, '2017_07_05_073658_create_business_table', 1),
(5, '2017_07_22_075923_add_business_id_users_table', 1),
(6, '2017_07_23_113209_create_brands_table', 1),
(7, '2017_07_26_083429_create_permission_tables', 1),
(8, '2017_07_26_110000_create_tax_rates_table', 1),
(9, '2017_07_26_122313_create_units_table', 1),
(10, '2017_07_27_075706_create_contacts_table', 1),
(11, '2017_08_04_071038_create_categories_table', 1),
(12, '2017_08_08_115903_create_products_table', 1),
(13, '2017_08_09_061616_create_variation_templates_table', 1),
(14, '2017_08_09_061638_create_variation_value_templates_table', 1),
(15, '2017_08_10_061146_create_product_variations_table', 1),
(16, '2017_08_10_061216_create_variations_table', 1),
(17, '2017_08_19_054827_create_transactions_table', 1),
(18, '2017_08_31_073533_create_purchase_lines_table', 1),
(19, '2017_10_15_064638_create_transaction_payments_table', 1),
(20, '2017_10_31_065621_add_default_sales_tax_to_business_table', 1),
(21, '2017_11_20_051930_create_table_group_sub_taxes', 1),
(22, '2017_11_20_063603_create_transaction_sell_lines', 1),
(23, '2017_11_21_064540_create_barcodes_table', 1),
(24, '2017_11_23_181237_create_invoice_schemes_table', 1),
(25, '2017_12_25_122822_create_business_locations_table', 1),
(26, '2017_12_25_160253_add_location_id_to_transactions_table', 1),
(27, '2017_12_25_163227_create_variation_location_details_table', 1),
(28, '2018_01_04_115627_create_sessions_table', 1),
(29, '2018_01_05_112817_create_invoice_layouts_table', 1),
(30, '2018_01_06_112303_add_invoice_scheme_id_and_invoice_layout_id_to_business_locations', 1),
(31, '2018_01_08_104124_create_expense_categories_table', 1),
(32, '2018_01_08_123327_modify_transactions_table_for_expenses', 1),
(33, '2018_01_09_111005_modify_payment_status_in_transactions_table', 1),
(34, '2018_01_09_111109_add_paid_on_column_to_transaction_payments_table', 1),
(35, '2018_01_25_172439_add_printer_related_fields_to_business_locations_table', 1),
(36, '2018_01_27_184322_create_printers_table', 1),
(37, '2018_01_30_181442_create_cash_registers_table', 1),
(38, '2018_01_31_125836_create_cash_register_transactions_table', 1),
(39, '2018_02_07_173326_modify_business_table', 1),
(40, '2018_02_08_105425_add_enable_product_expiry_column_to_business_table', 1),
(41, '2018_02_08_111027_add_expiry_period_and_expiry_period_type_columns_to_products_table', 1),
(42, '2018_02_08_131118_add_mfg_date_and_exp_date_purchase_lines_table', 1),
(43, '2018_02_08_155348_add_exchange_rate_to_transactions_table', 1),
(44, '2018_02_09_124945_modify_transaction_payments_table_for_contact_payments', 1),
(45, '2018_02_12_113640_create_transaction_sell_lines_purchase_lines_table', 1),
(46, '2018_02_12_114605_add_quantity_sold_in_purchase_lines_table', 1),
(47, '2018_02_13_183323_alter_decimal_fields_size', 1),
(48, '2018_02_14_161928_add_transaction_edit_days_to_business_table', 1),
(49, '2018_02_15_161032_add_document_column_to_transactions_table', 1),
(50, '2018_02_17_124709_add_more_options_to_invoice_layouts', 1),
(51, '2018_02_19_111517_add_keyboard_shortcut_column_to_business_table', 1),
(52, '2018_02_19_121537_stock_adjustment_move_to_transaction_table', 1),
(53, '2018_02_20_165505_add_is_direct_sale_column_to_transactions_table', 1),
(54, '2018_02_21_105329_create_system_table', 1),
(55, '2018_02_23_100549_version_1_2', 1),
(56, '2018_02_23_125648_add_enable_editing_sp_from_purchase_column_to_business_table', 1),
(57, '2018_02_26_103612_add_sales_commission_agent_column_to_business_table', 1),
(58, '2018_02_26_130519_modify_users_table_for_sales_cmmsn_agnt', 1),
(59, '2018_02_26_134500_add_commission_agent_to_transactions_table', 1),
(60, '2018_02_27_121422_add_item_addition_method_to_business_table', 1),
(61, '2018_02_27_170232_modify_transactions_table_for_stock_transfer', 1),
(62, '2018_03_05_153510_add_enable_inline_tax_column_to_business_table', 1),
(63, '2018_03_06_210206_modify_product_barcode_types', 1),
(64, '2018_03_13_181541_add_expiry_type_to_business_table', 1),
(65, '2018_03_16_113446_product_expiry_setting_for_business', 1),
(66, '2018_03_19_113601_add_business_settings_options', 1),
(67, '2018_03_26_125334_add_pos_settings_to_business_table', 1),
(68, '2018_03_26_165350_create_customer_groups_table', 1),
(69, '2018_03_27_122720_customer_group_related_changes_in_tables', 1),
(70, '2018_03_29_110138_change_tax_field_to_nullable_in_business_table', 1),
(71, '2018_03_29_115502_add_changes_for_sr_number_in_products_and_sale_lines_table', 1),
(72, '2018_03_29_134340_add_inline_discount_fields_in_purchase_lines', 1),
(73, '2018_03_31_140921_update_transactions_table_exchange_rate', 1),
(74, '2018_04_03_103037_add_contact_id_to_contacts_table', 1),
(75, '2018_04_03_122709_add_changes_to_invoice_layouts_table', 1),
(76, '2018_04_09_135320_change_exchage_rate_size_in_business_table', 1),
(77, '2018_04_17_123122_add_lot_number_to_business', 1),
(78, '2018_04_17_160845_add_product_racks_table', 1),
(79, '2018_04_20_182015_create_res_tables_table', 1),
(80, '2018_04_24_105246_restaurant_fields_in_transaction_table', 1),
(81, '2018_04_24_114149_add_enabled_modules_business_table', 1),
(82, '2018_04_24_133704_add_modules_fields_in_invoice_layout_table', 1),
(83, '2018_04_27_132653_quotation_related_change', 1),
(84, '2018_05_02_104439_add_date_format_and_time_format_to_business', 1),
(85, '2018_05_02_111939_add_sell_return_to_transaction_payments', 1),
(86, '2018_05_14_114027_add_rows_positions_for_products', 1),
(87, '2018_05_14_125223_add_weight_to_products_table', 1),
(88, '2018_05_14_164754_add_opening_stock_permission', 1),
(89, '2018_05_15_134729_add_design_to_invoice_layouts', 1),
(90, '2018_05_16_183307_add_tax_fields_invoice_layout', 1),
(91, '2018_05_18_191956_add_sell_return_to_transaction_table', 1),
(92, '2018_05_21_131349_add_custom_fileds_to_contacts_table', 1),
(93, '2018_05_21_131607_invoice_layout_fields_for_sell_return', 1),
(94, '2018_05_21_131949_add_custom_fileds_and_website_to_business_locations_table', 1),
(95, '2018_05_22_123527_create_reference_counts_table', 1),
(96, '2018_05_22_154540_add_ref_no_prefixes_column_to_business_table', 1),
(97, '2018_05_24_132620_add_ref_no_column_to_transaction_payments_table', 1),
(98, '2018_05_24_161026_add_location_id_column_to_business_location_table', 1),
(99, '2018_05_25_180603_create_modifiers_related_table', 1),
(100, '2018_05_29_121714_add_purchase_line_id_to_stock_adjustment_line_table', 1),
(101, '2018_05_31_114645_add_res_order_status_column_to_transactions_table', 1),
(102, '2018_06_05_103530_rename_purchase_line_id_in_stock_adjustment_lines_table', 1),
(103, '2018_06_05_111905_modify_products_table_for_modifiers', 1),
(104, '2018_06_06_110524_add_parent_sell_line_id_column_to_transaction_sell_lines_table', 1),
(105, '2018_06_07_152443_add_is_service_staff_to_roles_table', 1),
(106, '2018_06_07_182258_add_image_field_to_products_table', 1),
(107, '2018_06_13_133705_create_bookings_table', 1),
(108, '2018_06_15_173636_add_email_column_to_contacts_table', 1),
(109, '2018_06_27_182835_add_superadmin_related_fields_business', 1),
(110, '2018_07_10_101913_add_custom_fields_to_products_table', 1),
(111, '2018_07_17_103434_add_sales_person_name_label_to_invoice_layouts_table', 1),
(112, '2018_07_17_120612_change_all_quantity_field_type_to_decimal', 1),
(113, '2018_07_17_163920_add_theme_skin_color_column_to_business_table', 1),
(114, '2018_07_24_160319_add_lot_no_line_id_to_transaction_sell_lines_table', 1),
(115, '2018_07_25_110004_add_show_expiry_and_show_lot_colums_to_invoice_layouts_table', 1),
(116, '2018_07_25_172004_add_discount_columns_to_transaction_sell_lines_table', 1),
(117, '2018_07_26_124720_change_design_column_type_in_invoice_layouts_table', 1),
(118, '2018_07_26_170424_add_unit_price_before_discount_column_to_transaction_sell_line_table', 1),
(119, '2018_07_28_103614_add_credit_limit_column_to_contacts_table', 1),
(120, '2018_08_08_110755_add_new_payment_methods_to_transaction_payments_table', 1),
(121, '2018_08_08_122225_modify_cash_register_transactions_table_for_new_payment_methods', 1),
(122, '2018_08_14_104036_add_opening_balance_type_to_transactions_table', 1),
(123, '2018_09_06_114438_create_selling_price_groups_table', 1),
(124, '2018_09_06_154057_create_variation_group_prices_table', 1),
(125, '2018_09_07_102413_add_permission_to_access_default_selling_price', 1),
(126, '2018_09_07_134858_add_selling_price_group_id_to_transactions_table', 1),
(127, '2018_09_10_112448_update_product_type_to_single_if_null_in_products_table', 1),
(128, '2018_09_10_173656_add_account_id_column_to_transaction_payments_table', 1),
(129, '2018_09_19_123914_create_notification_templates_table', 1),
(130, '2018_09_22_110504_add_sms_and_email_settings_columns_to_business_table', 1),
(131, '2018_09_24_134942_add_lot_no_line_id_to_stock_adjustment_lines_table', 1),
(132, '2018_09_27_111609_modify_transactions_table_for_purchase_return', 1),
(133, '2018_09_27_131154_add_quantity_returned_column_to_purchase_lines_table', 1),
(134, '2018_10_02_131401_add_return_quantity_column_to_transaction_sell_lines_table', 1),
(135, '2018_10_03_104918_add_qty_returned_column_to_transaction_sell_lines_purchase_lines_table', 1),
(136, '2018_10_03_185947_add_default_notification_templates_to_database', 1),
(137, '2018_10_09_153105_add_business_id_to_transaction_payments_table', 1),
(138, '2018_10_16_135229_create_permission_for_sells_and_purchase', 1),
(139, '2018_10_22_114441_add_columns_for_variable_product_modifications', 1),
(140, '2018_10_22_134428_modify_variable_product_data', 1),
(141, '2018_10_30_181558_add_table_tax_headings_to_invoice_layout', 1),
(142, '2018_10_31_122619_add_pay_terms_field_transactions_table', 1),
(143, '2018_10_31_161328_add_new_permissions_for_pos_screen', 1),
(144, '2018_10_31_174752_add_access_selected_contacts_only_to_users_table', 1),
(145, '2018_10_31_175627_add_user_contact_access', 1),
(146, '2018_10_31_180559_add_auto_send_sms_column_to_notification_templates_table', 1),
(147, '2018_11_02_171949_change_card_type_column_to_varchar_in_transaction_payments_table', 1),
(148, '2018_11_08_105621_add_role_permissions', 1),
(149, '2018_06_27_185405_create_packages_table', 2),
(150, '2018_06_28_182803_create_subscriptions_table', 2),
(151, '2018_07_17_182021_add_rows_to_system_table', 2),
(152, '2018_07_19_131721_add_options_to_packages_table', 2),
(153, '2018_08_17_155534_add_min_termination_alert_days', 2),
(154, '2018_08_28_105945_add_business_based_username_settings_to_system_table', 2),
(155, '2018_08_30_105906_add_superadmin_communicator_logs_table', 2),
(156, '2018_11_02_130636_add_custom_permissions_to_packages_table', 2),
(157, '2018_11_05_161848_add_more_fields_to_packages_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(3, 2, 'App\\User'),
(5, 3, 'App\\User'),
(6, 4, 'App\\User'),
(7, 5, 'App\\User'),
(12, 8, 'App\\User'),
(14, 9, 'App\\User'),
(18, 11, 'App\\User'),
(20, 12, 'App\\User'),
(22, 13, 'App\\User'),
(24, 14, 'App\\User'),
(26, 15, 'App\\User'),
(28, 16, 'App\\User'),
(30, 17, 'App\\User'),
(31, 18, 'App\\User'),
(32, 19, 'App\\User'),
(32, 20, 'App\\User'),
(33, 21, 'App\\User'),
(34, 22, 'App\\User'),
(36, 23, 'App\\User'),
(35, 24, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `notification_templates`
--

DROP TABLE IF EXISTS `notification_templates`;
CREATE TABLE IF NOT EXISTS `notification_templates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `template_for` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` text COLLATE utf8mb4_unicode_ci,
  `sms_body` text COLLATE utf8mb4_unicode_ci,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_send` tinyint(1) NOT NULL DEFAULT '0',
  `auto_send_sms` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_templates`
--

INSERT INTO `notification_templates` (`id`, `business_id`, `template_for`, `email_body`, `sms_body`, `subject`, `auto_send`, `auto_send_sms`, `created_at`, `updated_at`) VALUES
(1, 1, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(2, 1, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(3, 1, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(4, 1, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(5, 1, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(6, 1, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(7, 1, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(8, 1, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(9, 2, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(10, 2, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(11, 2, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(12, 2, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(13, 2, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(14, 2, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(15, 2, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(16, 2, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(17, 3, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(18, 3, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(19, 3, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(20, 3, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(21, 3, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(22, 3, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(23, 3, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(24, 3, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(25, 4, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(26, 4, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(27, 4, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(28, 4, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(29, 4, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(30, 4, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(31, 4, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(32, 4, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(33, 5, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(34, 5, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(35, 5, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(36, 5, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(37, 5, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(38, 5, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(39, 5, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(40, 5, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(41, 6, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(42, 6, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(43, 6, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(44, 6, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(45, 6, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(46, 6, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(47, 6, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(48, 6, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(49, 7, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(50, 7, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(51, 7, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(52, 7, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(53, 7, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(54, 7, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(55, 7, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(56, 7, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(57, 8, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(58, 8, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(59, 8, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(60, 8, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(61, 8, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(62, 8, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(63, 8, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(64, 8, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(65, 9, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(66, 9, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(67, 9, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(68, 9, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(69, 9, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(70, 9, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(71, 9, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(72, 9, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(73, 10, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(74, 10, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(75, 10, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(76, 10, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(77, 10, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(78, 10, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(79, 10, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(80, 10, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(81, 11, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(82, 11, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(83, 11, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(84, 11, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(85, 11, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(86, 11, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(87, 11, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(88, 11, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(89, 12, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(90, 12, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(91, 12, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(92, 12, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(93, 12, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(94, 12, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(95, 12, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(96, 12, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(97, 13, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(98, 13, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(99, 13, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(100, 13, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40');
INSERT INTO `notification_templates` (`id`, `business_id`, `template_for`, `email_body`, `sms_body`, `subject`, `auto_send`, `auto_send_sms`, `created_at`, `updated_at`) VALUES
(101, 13, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(102, 13, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(103, 13, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(104, 13, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(105, 14, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(106, 14, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(107, 14, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(108, 14, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(109, 14, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(110, 14, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(111, 14, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(112, 14, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(113, 15, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(114, 15, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(115, 15, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(116, 15, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(117, 15, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(118, 15, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(119, 15, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(120, 15, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(121, 16, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(122, 16, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(123, 16, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(124, 16, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(125, 16, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(126, 16, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(127, 16, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(128, 16, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(129, 17, 'new_sale', '<p>Dear {contact_name},</p>\n\n                    <p>Your invoice number is {invoice_number}<br />\n                    Total amount: {total_amount}<br />\n                    Paid amount: {paid_amount}</p>\n\n                    <p>Thank you for shopping with us.</p>\n\n                    <p>{business_logo}</p>\n\n                    <p>&nbsp;</p>', 'Dear {contact_name}, Thank you for shopping with us. {business_name}', 'Thank you from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(130, 17, 'payment_received', '<p>Dear {contact_name},</p>\n\n                <p>We have received a payment of {paid_amount}</p>\n\n                <p>{business_logo}</p>', 'Dear {contact_name}, We have received a payment of {paid_amount}. {business_name}', 'Payment Received, from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(131, 17, 'payment_reminder', '<p>Dear {contact_name},</p>\n\n                    <p>This is to remind you that you have pending payment of {due_amount}. Kindly pay it as soon as possible.</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, You have pending payment of {due_amount}. Kindly pay it as soon as possible. {business_name}', 'Payment Reminder, from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(132, 17, 'new_booking', '<p>Dear {contact_name},</p>\n\n                    <p>Your booking is confirmed</p>\n\n                    <p>Date: {start_time} to {end_time}</p>\n\n                    <p>Table: {table}</p>\n\n                    <p>Location: {location}</p>\n\n                    <p>{business_logo}</p>', 'Dear {contact_name}, Your booking is confirmed. Date: {start_time} to {end_time}, Table: {table}, Location: {location}', 'Booking Confirmed - {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(133, 17, 'new_order', '<p>Dear {contact_name},</p>\n\n                    <p>We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'Dear {contact_name}, We have a new order with reference number {invoice_number}. Kindly process the products as soon as possible. {business_name}', 'New Order, from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(134, 17, 'payment_paid', '<p>Dear {contact_name},</p>\n\n                    <p>We have paid amount {paid_amount} again invoice number {invoice_number}.<br />\n                    Kindly note it down.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have paid amount {paid_amount} again invoice number {invoice_number}.\n                    Kindly note it down. {business_name}', 'Payment Paid, from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(135, 17, 'items_received', '<p>Dear {contact_name},</p>\n\n                    <p>We have received all items from invoice reference number {invoice_number}. Thank you for processing it.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'We have received all items from invoice reference number {invoice_number}. Thank you for processing it. {business_name}', 'Items received, from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(136, 17, 'items_pending', '<p>Dear {contact_name},<br />\n                    This is to remind you that we have not yet received some items from invoice reference number {invoice_number}. Please process it as soon as possible.</p>\n\n                    <p>{business_name}<br />\n                    {business_logo}</p>', 'This is to remind you that we have not yet received some items from invoice reference number {invoice_number} . Please process it as soon as possible.{business_name}', 'Items Pending, from {business_name}', 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_count` int(11) NOT NULL COMMENT 'No. of Business Locations, 0 = infinite option.',
  `user_count` int(11) NOT NULL,
  `product_count` int(11) NOT NULL,
  `bookings` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Enable/Disable bookings',
  `kitchen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Enable/Disable kitchen',
  `order_screen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Enable/Disable order_screen',
  `tables` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Enable/Disable tables',
  `invoice_count` int(11) NOT NULL,
  `interval` enum('days','months','years') COLLATE utf8mb4_unicode_ci NOT NULL,
  `interval_count` int(11) NOT NULL,
  `trial_days` int(11) NOT NULL,
  `price` decimal(20,4) NOT NULL,
  `custom_permissions` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT '0',
  `is_one_time` tinyint(1) NOT NULL DEFAULT '0',
  `enable_custom_link` tinyint(1) NOT NULL DEFAULT '0',
  `custom_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_link_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `description`, `location_count`, `user_count`, `product_count`, `bookings`, `kitchen`, `order_screen`, `tables`, `invoice_count`, `interval`, `interval_count`, `trial_days`, `price`, `custom_permissions`, `created_by`, `sort_order`, `is_active`, `is_private`, `is_one_time`, `enable_custom_link`, `custom_link`, `custom_link_text`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Free Trial', 'Simply enter your details to start your free 14-day trial.', 1, 1, 1000, 0, 0, 0, 0, 0, 'years', 1, 30, '5000.0000', '', 1, 1, 1, 0, 0, 0, '', '', NULL, '2018-11-26 12:34:23', '2018-11-27 14:24:56'),
(2, 'Trial1', 'simply start your multi location accounting', 2, 4, 1000, 0, 0, 0, 0, 0, 'years', 1, 30, '10000.0000', '', 1, 1, 1, 0, 0, 0, '', '', NULL, '2018-11-27 14:28:53', '2018-12-01 10:11:40'),
(3, 'Free trial electronins', 'Two business locations', 2, 6, 1000, 0, 0, 0, 0, 0, 'years', 1, 45, '20000.0000', '', 1, 1, 1, 0, 0, 0, '', '', NULL, '2018-11-28 07:06:46', '2018-11-28 07:06:46'),
(4, 'pakg1', 'kjkjkzj', 10, 100, 1000, 0, 0, 0, 0, 500, 'months', 1, 10, '100.0000', '', 1, 1, 1, 0, 0, 0, '', '', NULL, '2018-12-28 12:00:57', '2018-12-28 12:02:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('info@quadleo.com', '$2y$10$iduig8VOFoI0i9tSx2f/NuZfXjISHrb/UKg8BqKXHsX0qupUD53gm', '2018-11-26 12:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'profit_loss_report.view', 'web', '2018-11-14 07:00:34', NULL),
(2, 'direct_sell.access', 'web', '2018-11-14 07:00:34', NULL),
(3, 'product.opening_stock', 'web', '2018-11-14 07:08:35', '2018-11-14 07:08:35'),
(4, 'crud_all_bookings', 'web', '2018-11-14 07:09:39', '2018-11-14 07:09:39'),
(5, 'crud_own_bookings', 'web', '2018-11-14 07:09:40', '2018-11-14 07:09:40'),
(6, 'access_default_selling_price', 'web', '2018-11-14 07:11:27', '2018-11-14 07:11:27'),
(7, 'purchase.payments', 'web', '2018-11-14 07:11:50', '2018-11-14 07:11:50'),
(8, 'sell.payments', 'web', '2018-11-14 07:11:51', '2018-11-14 07:11:51'),
(9, 'edit_product_price_from_sale_screen', 'web', '2018-11-14 07:12:00', '2018-11-14 07:12:00'),
(10, 'edit_product_discount_from_sale_screen', 'web', '2018-11-14 07:12:00', '2018-11-14 07:12:00'),
(11, 'roles.view', 'web', '2018-11-14 07:12:09', '2018-11-14 07:12:09'),
(12, 'roles.create', 'web', '2018-11-14 07:12:09', '2018-11-14 07:12:09'),
(13, 'roles.update', 'web', '2018-11-14 07:12:09', '2018-11-14 07:12:09'),
(14, 'roles.delete', 'web', '2018-11-14 07:12:10', '2018-11-14 07:12:10'),
(15, 'user.view', 'web', '2018-11-14 07:12:11', NULL),
(16, 'user.create', 'web', '2018-11-14 07:12:11', NULL),
(17, 'user.update', 'web', '2018-11-14 07:12:11', NULL),
(18, 'user.delete', 'web', '2018-11-14 07:12:11', NULL),
(19, 'supplier.view', 'web', '2018-11-14 07:12:11', NULL),
(20, 'supplier.create', 'web', '2018-11-14 07:12:11', NULL),
(21, 'supplier.update', 'web', '2018-11-14 07:12:11', NULL),
(22, 'supplier.delete', 'web', '2018-11-14 07:12:11', NULL),
(23, 'customer.view', 'web', '2018-11-14 07:12:11', NULL),
(24, 'customer.create', 'web', '2018-11-14 07:12:11', NULL),
(25, 'customer.update', 'web', '2018-11-14 07:12:11', NULL),
(26, 'customer.delete', 'web', '2018-11-14 07:12:11', NULL),
(27, 'product.view', 'web', '2018-11-14 07:12:11', NULL),
(28, 'product.create', 'web', '2018-11-14 07:12:11', NULL),
(29, 'product.update', 'web', '2018-11-14 07:12:11', NULL),
(30, 'product.delete', 'web', '2018-11-14 07:12:11', NULL),
(31, 'purchase.view', 'web', '2018-11-14 07:12:11', NULL),
(32, 'purchase.create', 'web', '2018-11-14 07:12:11', NULL),
(33, 'purchase.update', 'web', '2018-11-14 07:12:11', NULL),
(34, 'purchase.delete', 'web', '2018-11-14 07:12:11', NULL),
(35, 'sell.view', 'web', '2018-11-14 07:12:11', NULL),
(36, 'sell.create', 'web', '2018-11-14 07:12:11', NULL),
(37, 'sell.update', 'web', '2018-11-14 07:12:11', NULL),
(38, 'sell.delete', 'web', '2018-11-14 07:12:11', NULL),
(39, 'purchase_n_sell_report.view', 'web', '2018-11-14 07:12:11', NULL),
(40, 'contacts_report.view', 'web', '2018-11-14 07:12:11', NULL),
(41, 'stock_report.view', 'web', '2018-11-14 07:12:11', NULL),
(42, 'tax_report.view', 'web', '2018-11-14 07:12:11', NULL),
(43, 'trending_product_report.view', 'web', '2018-11-14 07:12:11', NULL),
(44, 'register_report.view', 'web', '2018-11-14 07:12:11', NULL),
(45, 'sales_representative.view', 'web', '2018-11-14 07:12:11', NULL),
(46, 'expense_report.view', 'web', '2018-11-14 07:12:11', NULL),
(47, 'business_settings.access', 'web', '2018-11-14 07:12:11', NULL),
(48, 'barcode_settings.access', 'web', '2018-11-14 07:12:11', NULL),
(49, 'invoice_settings.access', 'web', '2018-11-14 07:12:11', NULL),
(50, 'brand.view', 'web', '2018-11-14 07:12:11', NULL),
(51, 'brand.create', 'web', '2018-11-14 07:12:11', NULL),
(52, 'brand.update', 'web', '2018-11-14 07:12:11', NULL),
(53, 'brand.delete', 'web', '2018-11-14 07:12:11', NULL),
(54, 'tax_rate.view', 'web', '2018-11-14 07:12:11', NULL),
(55, 'tax_rate.create', 'web', '2018-11-14 07:12:11', NULL),
(56, 'tax_rate.update', 'web', '2018-11-14 07:12:11', NULL),
(57, 'tax_rate.delete', 'web', '2018-11-14 07:12:11', NULL),
(58, 'unit.view', 'web', '2018-11-14 07:12:11', NULL),
(59, 'unit.create', 'web', '2018-11-14 07:12:11', NULL),
(60, 'unit.update', 'web', '2018-11-14 07:12:11', NULL),
(61, 'unit.delete', 'web', '2018-11-14 07:12:11', NULL),
(62, 'category.view', 'web', '2018-11-14 07:12:11', NULL),
(63, 'category.create', 'web', '2018-11-14 07:12:11', NULL),
(64, 'category.update', 'web', '2018-11-14 07:12:11', NULL),
(65, 'category.delete', 'web', '2018-11-14 07:12:11', NULL),
(66, 'expense.access', 'web', '2018-11-14 07:12:11', NULL),
(67, 'access_all_locations', 'web', '2018-11-14 07:12:11', NULL),
(68, 'dashboard.data', 'web', '2018-11-14 07:12:11', NULL),
(69, 'location.1', 'web', '2018-11-14 07:52:41', '2018-11-14 07:52:41'),
(70, 'location.2', 'web', '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(71, 'location.3', 'web', '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(72, 'location.4', 'web', '2018-11-27 14:40:01', '2018-11-27 14:40:01'),
(73, 'location.5', 'web', '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(74, 'location.6', 'web', '2018-11-30 11:29:36', '2018-11-30 11:29:36'),
(75, 'location.7', 'web', '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(76, 'location.8', 'web', '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(77, 'location.9', 'web', '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(78, 'location.10', 'web', '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(79, 'location.11', 'web', '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(80, 'location.12', 'web', '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(81, 'location.13', 'web', '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(82, 'location.14', 'web', '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(83, 'location.15', 'web', '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(84, 'location.16', 'web', '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(85, 'location.17', 'web', '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(86, 'location.18', 'web', '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(87, 'location.19', 'web', '2019-02-08 05:33:52', '2019-02-08 05:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

DROP TABLE IF EXISTS `printers`;
CREATE TABLE IF NOT EXISTS `printers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection_type` enum('network','windows','linux') COLLATE utf8mb4_unicode_ci NOT NULL,
  `capability_profile` enum('default','simple','SP2000','TEP-200M','P822D') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `char_per_line` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `printers_business_id_foreign` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `type` enum('single','variable','modifier') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_id` int(11) UNSIGNED DEFAULT NULL,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sub_category_id` int(10) UNSIGNED DEFAULT NULL,
  `tax` int(10) UNSIGNED DEFAULT NULL,
  `tax_type` enum('inclusive','exclusive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable_stock` tinyint(1) NOT NULL DEFAULT '0',
  `alert_quantity` int(11) NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode_type` enum('C39','C128','EAN13','EAN8','UPCA','UPCE') COLLATE utf8mb4_unicode_ci DEFAULT 'C128',
  `expiry_period` decimal(4,2) DEFAULT NULL,
  `expiry_period_type` enum('days','months') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_sr_no` tinyint(1) NOT NULL DEFAULT '0',
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_custom_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_custom_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_custom_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_custom_field4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_brand_id_foreign` (`brand_id`),
  KEY `products_category_id_foreign` (`category_id`),
  KEY `products_sub_category_id_foreign` (`sub_category_id`),
  KEY `products_tax_foreign` (`tax`),
  KEY `products_name_index` (`name`),
  KEY `products_business_id_index` (`business_id`),
  KEY `products_unit_id_index` (`unit_id`),
  KEY `products_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `business_id`, `type`, `unit_id`, `brand_id`, `category_id`, `sub_category_id`, `tax`, `tax_type`, `enable_stock`, `alert_quantity`, `sku`, `barcode_type`, `expiry_period`, `expiry_period_type`, `enable_sr_no`, `weight`, `product_custom_field1`, `product_custom_field2`, `product_custom_field3`, `product_custom_field4`, `image`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'digital', 3, 'single', 3, 1, 1, NULL, NULL, 'exclusive', 1, 10, '0001', 'C128', NULL, NULL, 0, '300gm', NULL, NULL, NULL, NULL, NULL, 3, '2018-11-28 06:17:31', '2018-11-28 06:17:31'),
(2, '3w bulb', 4, 'single', 4, 3, NULL, NULL, NULL, 'inclusive', 1, 10, '0002', 'C128', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2018-11-30 11:36:36', '2018-11-30 11:36:36'),
(3, 'A2A 90 DAYS', 10, 'single', 10, NULL, NULL, NULL, NULL, 'exclusive', 1, 1, 'A2A', 'C128', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 12, '2018-12-12 12:11:06', '2018-12-12 12:11:06'),
(4, 'VISIT VISA 90 DAYS', 10, 'single', 10, NULL, NULL, NULL, NULL, 'inclusive', 1, 1, '0004', 'C128', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 12, '2018-12-12 12:20:39', '2018-12-12 12:20:39'),
(5, 'demo product', 2, 'single', 2, 10, NULL, NULL, NULL, 'exclusive', 1, 3, '0005', 'C128', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2019-01-25 09:17:16', '2019-01-25 09:17:16'),
(6, 'I phone 7', 17, 'single', 19, 12, 7, 10, 2, 'inclusive', 1, 3, '0006', 'C128', NULL, NULL, 1, '3', NULL, NULL, NULL, NULL, NULL, 22, '2019-02-08 06:04:50', '2019-02-08 06:04:50'),
(7, 'i phone 10', 17, 'single', 19, 12, 7, 10, NULL, 'exclusive', 1, 3, '0007', 'C128', NULL, NULL, 1, '2', NULL, NULL, NULL, NULL, NULL, 22, '2019-02-12 05:30:18', '2019-02-12 05:30:18'),
(8, 'i phone 6', 17, 'single', 19, 12, 7, 10, NULL, 'exclusive', 1, 3, '0008', 'C128', NULL, NULL, 0, '2', NULL, NULL, NULL, NULL, NULL, 22, '2019-02-12 05:31:20', '2019-02-12 05:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `product_racks`
--

DROP TABLE IF EXISTS `product_racks`;
CREATE TABLE IF NOT EXISTS `product_racks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `rack` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

DROP TABLE IF EXISTS `product_variations`;
CREATE TABLE IF NOT EXISTS `product_variations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variation_template_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `is_dummy` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_variations_name_index` (`name`),
  KEY `product_variations_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variations`
--

INSERT INTO `product_variations` (`id`, `variation_template_id`, `name`, `product_id`, `is_dummy`, `created_at`, `updated_at`) VALUES
(1, NULL, 'DUMMY', 1, 1, '2018-11-28 06:17:31', '2018-11-28 06:17:31'),
(2, NULL, 'DUMMY', 2, 1, '2018-11-30 11:36:36', '2018-11-30 11:36:36'),
(3, NULL, 'DUMMY', 3, 1, '2018-12-12 12:11:06', '2018-12-12 12:11:06'),
(4, NULL, 'DUMMY', 4, 1, '2018-12-12 12:20:39', '2018-12-12 12:20:39'),
(5, NULL, 'DUMMY', 5, 1, '2019-01-25 09:17:16', '2019-01-25 09:17:16'),
(6, NULL, 'DUMMY', 6, 1, '2019-02-08 06:04:50', '2019-02-08 06:04:50'),
(7, NULL, 'DUMMY', 7, 1, '2019-02-12 05:30:18', '2019-02-12 05:30:18'),
(8, NULL, 'DUMMY', 8, 1, '2019-02-12 05:31:20', '2019-02-12 05:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_lines`
--

DROP TABLE IF EXISTS `purchase_lines`;
CREATE TABLE IF NOT EXISTS `purchase_lines` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `variation_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(20,4) NOT NULL,
  `pp_without_discount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'Purchase price before inline discounts',
  `discount_percent` decimal(5,2) NOT NULL DEFAULT '0.00' COMMENT 'Inline discount percentage',
  `purchase_price` decimal(20,2) DEFAULT NULL,
  `purchase_price_inc_tax` decimal(20,2) NOT NULL DEFAULT '0.00',
  `item_tax` decimal(20,2) DEFAULT NULL,
  `tax_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity_sold` decimal(20,4) DEFAULT '0.0000',
  `quantity_adjusted` decimal(20,4) DEFAULT '0.0000',
  `quantity_returned` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `mfg_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `lot_number` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_lines_transaction_id_foreign` (`transaction_id`),
  KEY `purchase_lines_product_id_foreign` (`product_id`),
  KEY `purchase_lines_variation_id_foreign` (`variation_id`),
  KEY `purchase_lines_tax_id_foreign` (`tax_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_lines`
--

INSERT INTO `purchase_lines` (`id`, `transaction_id`, `product_id`, `variation_id`, `quantity`, `pp_without_discount`, `discount_percent`, `purchase_price`, `purchase_price_inc_tax`, `item_tax`, `tax_id`, `quantity_sold`, `quantity_adjusted`, `quantity_returned`, `mfg_date`, `exp_date`, `lot_number`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '1000.0000', '1500.00', '0.00', '1500.00', '1500.00', '0.00', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2018-11-28 06:21:39', '2018-11-28 06:21:39'),
(2, 2, 1, 1, '100.0000', '1500.00', '0.00', '1500.00', '1500.00', '0.00', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2018-11-28 06:22:47', '2018-11-28 06:22:47'),
(3, 3, 2, 2, '11.0000', '80.00', '0.00', '80.00', '80.00', '0.00', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2018-11-30 11:36:58', '2018-11-30 11:36:58'),
(4, 4, 2, 2, '12.0000', '80.00', '0.00', '80.00', '80.00', '0.00', NULL, '2.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2018-11-30 11:36:58', '2018-11-30 11:39:32'),
(5, 6, 3, 3, '1.0000', '1785.00', '0.00', '1785.00', '1785.00', '0.00', NULL, '1.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2018-12-12 12:13:42', '2018-12-12 12:15:49'),
(6, 11, 5, 5, '10.0000', '3000.00', '0.00', '3000.00', '3000.00', '0.00', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2019-01-25 09:17:32', '2019-01-25 09:17:32'),
(7, 12, 6, 6, '100.0000', '50000.00', '0.00', '50000.00', '52500.00', '2500.00', 2, '30.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2019-02-08 06:06:06', '2019-02-15 06:43:39'),
(8, 12, 6, 6, '3.0000', '47000.00', '0.00', '47000.00', '49350.00', '2350.00', 2, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2019-02-08 06:06:06', '2019-02-08 06:06:06'),
(9, 32, 7, 7, '100.0000', '77000.00', '0.00', '77000.00', '77000.00', '0.00', NULL, '63.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2019-02-12 09:17:35', '2019-02-15 06:43:39'),
(10, 33, 8, 8, '50.0000', '50000.00', '0.00', '50000.00', '50000.00', '0.00', NULL, '4.0000', '0.0000', '0.0000', NULL, NULL, NULL, '2019-02-12 09:17:50', '2019-02-15 05:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `reference_counts`
--

DROP TABLE IF EXISTS `reference_counts`;
CREATE TABLE IF NOT EXISTS `reference_counts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ref_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_count` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reference_counts`
--

INSERT INTO `reference_counts` (`id`, `ref_type`, `ref_count`, `business_id`, `created_at`, `updated_at`) VALUES
(1, 'contacts', 1, 1, '2018-11-14 07:52:39', '2018-11-14 07:52:39'),
(2, 'business_location', 1, 1, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(3, 'contacts', 1, 2, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(4, 'business_location', 1, 2, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(5, 'contacts', 2, 3, '2018-11-27 14:32:23', '2018-11-28 06:19:19'),
(6, 'business_location', 2, 3, '2018-11-27 14:32:23', '2018-11-27 14:40:01'),
(7, 'purchase', 2, 3, '2018-11-28 06:21:39', '2018-11-28 06:22:47'),
(8, 'purchase_payment', 2, 3, '2018-11-28 06:21:39', '2018-11-28 06:22:47'),
(9, 'contacts', 2, 4, '2018-11-28 07:25:40', '2018-11-30 11:32:16'),
(10, 'business_location', 2, 4, '2018-11-28 07:25:40', '2018-11-30 11:29:36'),
(11, 'sell_payment', 1, 4, '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(12, 'contacts', 1, 5, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(13, 'business_location', 1, 5, '2018-12-01 10:10:00', '2018-12-01 10:10:00'),
(14, 'contacts', 1, 6, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(15, 'business_location', 1, 6, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(16, 'contacts', 1, 7, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(17, 'business_location', 1, 7, '2018-12-11 07:57:43', '2018-12-11 07:57:43'),
(18, 'contacts', 1, 8, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(19, 'business_location', 1, 8, '2018-12-11 11:54:04', '2018-12-11 11:54:04'),
(20, 'contacts', 1, 9, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(21, 'business_location', 1, 9, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(22, 'contacts', 4, 10, '2018-12-12 13:09:05', '2018-12-12 12:15:03'),
(23, 'business_location', 1, 10, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(24, 'purchase', 1, 10, '2018-12-12 12:13:42', '2018-12-12 12:13:42'),
(25, 'sell_payment', 1, 10, '2018-12-12 12:15:49', '2018-12-12 12:15:49'),
(26, 'expense', 1, 10, '2018-12-13 13:08:03', '2018-12-13 13:08:03'),
(27, 'contacts', 1, 11, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(28, 'business_location', 1, 11, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(29, 'contacts', 1, 12, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(30, 'business_location', 1, 12, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(31, 'contacts', 1, 13, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(32, 'business_location', 1, 13, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(33, 'contacts', 1, 14, '2018-12-28 12:10:20', '2018-12-28 12:10:20'),
(34, 'business_location', 1, 14, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(35, 'contacts', 1, 15, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(36, 'business_location', 1, 15, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(37, 'contacts', 3, 16, '2019-01-16 07:38:33', '2019-01-16 09:46:43'),
(38, 'business_location', 1, 16, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(39, 'opening_balance', 2, 16, '2019-01-16 09:34:05', '2019-01-16 09:46:43'),
(40, 'contacts', 2, 17, '2019-02-08 05:33:52', '2019-02-08 06:15:26'),
(41, 'business_location', 1, 17, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(42, 'sell_payment', 112, 17, '2019-02-08 06:19:54', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `res_product_modifier_sets`
--

DROP TABLE IF EXISTS `res_product_modifier_sets`;
CREATE TABLE IF NOT EXISTS `res_product_modifier_sets` (
  `modifier_set_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'Table use to store the modifier sets applicable for a product',
  KEY `res_product_modifier_sets_modifier_set_id_foreign` (`modifier_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `res_tables`
--

DROP TABLE IF EXISTS `res_tables`;
CREATE TABLE IF NOT EXISTS `res_tables` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `res_tables_business_id_foreign` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_service_staff` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `business_id`, `is_default`, `is_service_staff`, `created_at`, `updated_at`) VALUES
(1, 'Admin#1', 'web', 1, 1, 0, '2018-11-14 07:52:38', '2018-11-14 07:52:38'),
(2, 'Cashier#1', 'web', 1, 0, 0, '2018-11-14 07:52:39', '2018-11-14 07:52:39'),
(3, 'Admin#2', 'web', 2, 1, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(4, 'Cashier#2', 'web', 2, 0, 0, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(5, 'Admin#3', 'web', 3, 1, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(6, 'Cashier#3', 'web', 3, 0, 0, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(7, 'Admin#4', 'web', 4, 1, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(8, 'Cashier#4', 'web', 4, 0, 0, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(9, 'manager#4', 'web', 4, 0, 0, '2018-11-28 08:01:00', '2018-11-28 08:01:00'),
(12, 'Admin#6', 'web', 6, 1, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(13, 'Cashier#6', 'web', 6, 0, 0, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(14, 'Admin#7', 'web', 7, 1, 0, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(15, 'Cashier#7', 'web', 7, 0, 0, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(18, 'Admin#9', 'web', 9, 1, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(19, 'Cashier#9', 'web', 9, 0, 0, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(20, 'Admin#10', 'web', 10, 1, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(21, 'Cashier#10', 'web', 10, 0, 0, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(22, 'Admin#11', 'web', 11, 1, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(23, 'Cashier#11', 'web', 11, 0, 0, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(24, 'Admin#12', 'web', 12, 1, 0, '2018-12-28 10:56:24', '2018-12-28 10:56:24'),
(25, 'Cashier#12', 'web', 12, 0, 0, '2018-12-28 10:56:24', '2018-12-28 10:56:24'),
(26, 'Admin#13', 'web', 13, 1, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(27, 'Cashier#13', 'web', 13, 0, 0, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(28, 'Admin#14', 'web', 14, 1, 0, '2018-12-28 12:10:17', '2018-12-28 12:10:17'),
(29, 'Cashier#14', 'web', 14, 0, 0, '2018-12-28 12:10:20', '2018-12-28 12:10:20'),
(30, 'Admin#15', 'web', 15, 1, 0, '2018-12-28 12:21:08', '2018-12-28 12:21:08'),
(31, 'Cashier#15', 'web', 15, 0, 0, '2018-12-28 12:21:08', '2018-12-28 12:21:08'),
(32, 'Admin#16', 'web', 16, 1, 0, '2019-01-16 07:38:32', '2019-01-16 07:38:32'),
(33, 'Cashier#16', 'web', 16, 0, 0, '2019-01-16 07:38:32', '2019-01-16 07:38:32'),
(34, 'Admin#17', 'web', 17, 1, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(35, 'Cashier#17', 'web', 17, 0, 0, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(36, 'testr_role#17', 'web', 17, 0, 0, '2019-02-08 09:27:40', '2019-02-08 09:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(67, 2),
(35, 4),
(36, 4),
(37, 4),
(38, 4),
(67, 4),
(35, 6),
(36, 6),
(37, 6),
(38, 6),
(67, 6),
(35, 8),
(36, 8),
(37, 8),
(38, 8),
(67, 8),
(1, 9),
(2, 9),
(3, 9),
(7, 9),
(8, 9),
(9, 9),
(10, 9),
(11, 9),
(15, 9),
(19, 9),
(20, 9),
(21, 9),
(22, 9),
(23, 9),
(24, 9),
(25, 9),
(26, 9),
(27, 9),
(28, 9),
(29, 9),
(30, 9),
(31, 9),
(32, 9),
(33, 9),
(34, 9),
(35, 9),
(36, 9),
(37, 9),
(38, 9),
(39, 9),
(40, 9),
(41, 9),
(42, 9),
(43, 9),
(44, 9),
(45, 9),
(46, 9),
(50, 9),
(51, 9),
(52, 9),
(53, 9),
(58, 9),
(59, 9),
(60, 9),
(61, 9),
(62, 9),
(63, 9),
(64, 9),
(65, 9),
(67, 9),
(68, 9),
(35, 13),
(36, 13),
(37, 13),
(38, 13),
(67, 13),
(35, 15),
(36, 15),
(37, 15),
(38, 15),
(67, 15),
(35, 19),
(36, 19),
(37, 19),
(38, 19),
(67, 19),
(35, 21),
(36, 21),
(37, 21),
(38, 21),
(67, 21),
(35, 23),
(36, 23),
(37, 23),
(38, 23),
(67, 23),
(35, 25),
(36, 25),
(37, 25),
(38, 25),
(67, 25),
(35, 27),
(36, 27),
(37, 27),
(38, 27),
(67, 27),
(35, 29),
(36, 29),
(37, 29),
(38, 29),
(67, 29),
(35, 31),
(36, 31),
(37, 31),
(38, 31),
(67, 31),
(35, 33),
(36, 33),
(37, 33),
(38, 33),
(67, 33),
(35, 35),
(36, 35),
(37, 35),
(38, 35),
(62, 35),
(63, 35),
(64, 35),
(65, 35),
(67, 35),
(67, 36),
(68, 36);

-- --------------------------------------------------------

--
-- Table structure for table `selling_price_groups`
--

DROP TABLE IF EXISTS `selling_price_groups`;
CREATE TABLE IF NOT EXISTS `selling_price_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `business_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `selling_price_groups_business_id_foreign` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stock_adjustments_temp`
--

DROP TABLE IF EXISTS `stock_adjustments_temp`;
CREATE TABLE IF NOT EXISTS `stock_adjustments_temp` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_adjustment_lines`
--

DROP TABLE IF EXISTS `stock_adjustment_lines`;
CREATE TABLE IF NOT EXISTS `stock_adjustment_lines` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `variation_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(20,4) NOT NULL,
  `unit_price` decimal(20,2) DEFAULT NULL COMMENT 'Last purchase unit price',
  `removed_purchase_line` int(11) DEFAULT NULL,
  `lot_no_line_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_adjustment_lines_product_id_foreign` (`product_id`),
  KEY `stock_adjustment_lines_variation_id_foreign` (`variation_id`),
  KEY `stock_adjustment_lines_transaction_id_index` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `start_date` date DEFAULT NULL,
  `trial_end_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `package_price` decimal(20,2) NOT NULL,
  `package_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_id` int(10) UNSIGNED NOT NULL,
  `paid_via` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('approved','waiting','declined') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `business_id`, `package_id`, `start_date`, `trial_end_date`, `end_date`, `package_price`, `package_details`, `created_id`, `paid_via`, `payment_transaction_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-11-26', '2019-12-26', '2019-11-26', '5000.00', '{\"location_count\":1,\"user_count\":1,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Free Trial\"}', 1, 'offline', '000', 'approved', NULL, '2018-11-26 12:38:09', '2018-11-26 12:38:09'),
(2, 3, 2, '2018-11-27', '2019-12-27', '2019-11-27', '10000.00', '{\"location_count\":2,\"user_count\":2,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Trial1\"}', 3, 'offline', NULL, 'approved', NULL, '2018-11-27 14:34:56', '2018-11-27 14:36:18'),
(3, 4, 3, '2018-11-28', '2020-01-12', '2019-11-28', '20000.00', '{\"location_count\":2,\"user_count\":6,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Free trial electronins\"}', 1, 'offline', NULL, 'approved', NULL, '2018-11-28 07:28:03', '2018-11-28 07:28:03'),
(6, 6, 2, '2018-12-03', '2020-01-02', '2019-12-03', '10000.00', '{\"location_count\":2,\"user_count\":4,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Trial1\"}', 8, 'offline', NULL, 'approved', NULL, '2018-12-03 08:19:03', '2018-12-03 08:19:31'),
(7, 7, 2, '2018-12-11', '2020-01-10', '2019-12-11', '10000.00', '{\"location_count\":2,\"user_count\":4,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Trial1\"}', 9, 'offline', NULL, 'approved', NULL, '2018-12-11 05:33:33', '2018-12-11 08:04:04'),
(8, 9, 2, '2018-12-11', '2020-01-10', '2019-12-11', '10000.00', '{\"location_count\":2,\"user_count\":4,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Trial1\"}', 11, 'offline', NULL, 'approved', NULL, '2018-12-11 10:31:27', '2018-12-11 12:01:47'),
(9, 10, 1, '2018-12-12', '2020-01-11', '2019-12-12', '5000.00', '{\"location_count\":1,\"user_count\":1,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Free Trial\"}', 12, 'offline', NULL, 'approved', NULL, '2018-12-12 11:43:26', '2018-12-12 13:13:54'),
(10, 13, 1, '2018-12-28', '2020-01-27', '2019-12-28', '5000.00', '{\"location_count\":1,\"user_count\":1,\"product_count\":1000,\"invoice_count\":0,\"name\":\"Free Trial\"}', 1, 'offline', '111', 'approved', NULL, '2018-12-28 10:59:34', '2018-12-28 10:59:34'),
(11, 15, 4, '2018-12-28', '2019-02-07', '2019-01-28', '100.00', '{\"location_count\":10,\"user_count\":100,\"product_count\":1000,\"invoice_count\":500,\"name\":\"pakg1\"}', 1, 'offline', '777', 'approved', NULL, '2018-12-28 12:48:11', '2018-12-28 12:48:11'),
(12, 16, 4, '2019-01-16', '2019-02-26', '2019-02-16', '100.00', '{\"location_count\":10,\"user_count\":100,\"product_count\":1000,\"invoice_count\":500,\"name\":\"pakg1\"}', 1, 'offline', '786', 'approved', NULL, '2019-01-16 07:39:16', '2019-01-16 07:39:16'),
(13, 17, 4, '2019-02-08', '2019-03-18', '2019-03-08', '100.00', '{\"location_count\":10,\"user_count\":100,\"product_count\":1000,\"invoice_count\":500,\"name\":\"pakg1\"}', 1, 'offline', '123', 'approved', NULL, '2019-02-08 05:37:32', '2019-02-08 05:37:32');

-- --------------------------------------------------------

--
-- Table structure for table `superadmin_communicator_logs`
--

DROP TABLE IF EXISTS `superadmin_communicator_logs`;
CREATE TABLE IF NOT EXISTS `superadmin_communicator_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_ids` text COLLATE utf8mb4_unicode_ci,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

DROP TABLE IF EXISTS `system`;
CREATE TABLE IF NOT EXISTS `system` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`key`, `value`) VALUES
('db_version', '2.11'),
('default_business_active_status', '1'),
('superadmin_version', '0.8'),
('app_currency_id', '53'),
('invoice_business_name', 'BillbixPOS'),
('invoice_business_landmark', 'ULCP'),
('invoice_business_zip', '673016'),
('invoice_business_state', 'Kerala'),
('invoice_business_city', 'Kozhikode'),
('invoice_business_country', 'India'),
('email', 'vipin@quadleo.com'),
('package_expiry_alert_days', '5'),
('enable_business_based_username', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
CREATE TABLE IF NOT EXISTS `tax_rates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `is_tax_group` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tax_rates_business_id_foreign` (`business_id`),
  KEY `tax_rates_created_by_foreign` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tax_rates`
--

INSERT INTO `tax_rates` (`id`, `business_id`, `name`, `amount`, `is_tax_group`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'VAT', 5.00, 0, 1, NULL, '2018-11-27 09:47:09', '2018-11-27 09:47:09'),
(2, 17, 'gst', 5.00, 0, 22, NULL, '2019-02-08 05:53:22', '2019-02-08 05:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `res_table_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'fields to restaurant module',
  `res_waiter_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'fields to restaurant module',
  `res_order_status` enum('received','cooked','served') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('purchase','sell','expense','stock_adjustment','sell_transfer','purchase_transfer','opening_stock','sell_return','opening_balance','purchase_return') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('received','pending','ordered','draft','final') COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_quotation` tinyint(1) NOT NULL DEFAULT '0',
  `payment_status` enum('paid','due','partial') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adjustment_type` enum('normal','abnormal') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_id` int(11) UNSIGNED DEFAULT NULL,
  `customer_group_id` int(11) DEFAULT NULL COMMENT 'used to add customer group while selling',
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_date` datetime NOT NULL,
  `total_before_tax` decimal(20,2) NOT NULL DEFAULT '0.00',
  `tax_id` int(10) UNSIGNED DEFAULT NULL,
  `tax_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `discount_type` enum('fixed','percentage') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_charges` decimal(20,2) NOT NULL DEFAULT '0.00',
  `additional_notes` text COLLATE utf8mb4_unicode_ci,
  `staff_note` text COLLATE utf8mb4_unicode_ci,
  `final_total` decimal(20,2) NOT NULL DEFAULT '0.00',
  `expense_category_id` int(10) UNSIGNED DEFAULT NULL,
  `expense_for` int(10) UNSIGNED DEFAULT NULL,
  `commission_agent` int(11) DEFAULT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_direct_sale` tinyint(1) NOT NULL DEFAULT '0',
  `exchange_rate` decimal(20,3) NOT NULL DEFAULT '1.000',
  `total_amount_recovered` decimal(20,2) DEFAULT NULL COMMENT 'Used for stock adjustment.',
  `transfer_parent_id` int(11) DEFAULT NULL,
  `return_parent_id` int(11) DEFAULT NULL,
  `opening_stock_product_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `pay_term_number` int(11) DEFAULT NULL,
  `pay_term_type` enum('days','months') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selling_price_group_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_tax_id_foreign` (`tax_id`),
  KEY `transactions_business_id_index` (`business_id`),
  KEY `transactions_type_index` (`type`),
  KEY `transactions_contact_id_index` (`contact_id`),
  KEY `transactions_transaction_date_index` (`transaction_date`),
  KEY `transactions_created_by_index` (`created_by`),
  KEY `transactions_location_id_index` (`location_id`),
  KEY `transactions_expense_for_foreign` (`expense_for`),
  KEY `transactions_expense_category_id_index` (`expense_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `business_id`, `location_id`, `res_table_id`, `res_waiter_id`, `res_order_status`, `type`, `status`, `is_quotation`, `payment_status`, `adjustment_type`, `contact_id`, `customer_group_id`, `invoice_no`, `ref_no`, `transaction_date`, `total_before_tax`, `tax_id`, `tax_amount`, `discount_type`, `discount_amount`, `shipping_details`, `shipping_charges`, `additional_notes`, `staff_note`, `final_total`, `expense_category_id`, `expense_for`, `commission_agent`, `document`, `is_direct_sale`, `exchange_rate`, `total_amount_recovered`, `transfer_parent_id`, `return_parent_id`, `opening_stock_product_id`, `created_by`, `pay_term_number`, `pay_term_type`, `selling_price_group_id`, `created_at`, `updated_at`) VALUES
(1, 3, 3, NULL, NULL, NULL, 'purchase', 'received', 0, 'partial', NULL, 4, NULL, NULL, 'PO2018/0001', '2018-11-28 00:00:00', '1500000.00', NULL, '0.00', NULL, '0', NULL, '0.00', NULL, NULL, '1500000.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2018-11-28 06:21:39', '2018-11-28 06:21:39'),
(2, 3, 4, NULL, NULL, NULL, 'purchase', 'received', 0, 'partial', NULL, 4, NULL, NULL, 'PO2018/0002', '2018-11-28 00:00:00', '150000.00', NULL, '0.00', NULL, '0', NULL, '0.00', NULL, NULL, '150000.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2018-11-28 06:22:47', '2018-11-28 06:22:47'),
(3, 4, 5, NULL, NULL, NULL, 'opening_stock', 'received', 0, 'paid', NULL, NULL, NULL, NULL, NULL, '2018-01-01 17:06:58', '880.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '880.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, 2, 5, NULL, NULL, NULL, '2018-11-30 11:36:58', '2018-11-30 11:36:58'),
(4, 4, 6, NULL, NULL, NULL, 'opening_stock', 'received', 0, 'paid', NULL, NULL, NULL, NULL, NULL, '2018-01-01 17:06:58', '960.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '960.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, 2, 5, NULL, NULL, NULL, '2018-11-30 11:36:58', '2018-11-30 11:36:58'),
(5, 4, 6, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 5, NULL, '0001', '', '2018-11-30 17:09:32', '200.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '200.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 5, NULL, NULL, 0, '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(6, 10, 12, NULL, NULL, NULL, 'purchase', 'received', 0, 'due', NULL, 14, NULL, NULL, 'PO2018/0001', '2018-12-01 00:00:00', '1785.00', NULL, '0.00', NULL, '0', NULL, '0.00', NULL, NULL, '1785.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, '2018-12-12 12:13:42', '2018-12-12 12:13:42'),
(7, 10, 12, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 15, NULL, '0001', '', '2018-12-01 17:43:00', '1850.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '1850.00', NULL, NULL, NULL, NULL, 1, '1.000', NULL, NULL, NULL, NULL, 12, NULL, NULL, 0, '2018-12-12 12:15:49', '2018-12-12 12:15:49'),
(8, 10, 12, NULL, NULL, NULL, 'expense', 'final', 0, 'due', NULL, NULL, NULL, NULL, '123123', '2018-12-13 00:00:00', '0.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '100.00', NULL, 12, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, '2018-12-13 13:08:03', '2018-12-13 13:08:03'),
(9, 16, 18, NULL, NULL, NULL, 'opening_balance', 'final', 0, 'due', NULL, 22, NULL, NULL, '2019/0001', '2019-01-16 15:04:05', '500.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '500.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, '2019-01-16 09:34:05', '2019-01-16 09:34:05'),
(10, 16, 18, NULL, NULL, NULL, 'opening_balance', 'final', 0, 'due', NULL, 23, NULL, NULL, '2019/0002', '2019-01-16 15:16:43', '10000.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '10000.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, '2019-01-16 09:46:43', '2019-01-16 09:46:43'),
(11, 2, 2, NULL, NULL, NULL, 'opening_stock', 'received', 0, 'paid', NULL, NULL, NULL, NULL, NULL, '2019-01-01 14:47:32', '30000.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '30000.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, 5, 2, NULL, NULL, NULL, '2019-01-25 09:17:32', '2019-01-25 09:17:32'),
(12, 17, 19, NULL, NULL, NULL, 'opening_stock', 'received', 0, 'paid', NULL, NULL, NULL, NULL, NULL, '2019-01-01 11:36:06', '5398050.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '5398050.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, 6, 22, NULL, NULL, NULL, '2019-02-08 06:06:06', '2019-02-08 06:06:06'),
(32, 17, 19, NULL, NULL, NULL, 'opening_stock', 'received', 0, 'paid', NULL, NULL, NULL, NULL, NULL, '2019-01-01 14:47:35', '7700000.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '7700000.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, 7, 22, NULL, NULL, NULL, '2019-02-12 09:17:35', '2019-02-12 09:17:35'),
(33, 17, 19, NULL, NULL, NULL, 'opening_stock', 'received', 0, 'paid', NULL, NULL, NULL, NULL, NULL, '2019-01-01 14:47:50', '2500000.00', NULL, '0.00', NULL, NULL, NULL, '0.00', NULL, NULL, '2500000.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, 8, 22, NULL, NULL, NULL, '2019-02-12 09:17:50', '2019-02-12 09:17:50'),
(98, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0031', '', '2019-02-14 12:45:44', '288750.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '288750.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 07:15:44', '2019-02-14 07:15:44'),
(99, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0032', '', '2019-02-14 12:46:16', '96250.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '96250.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 07:16:16', '2019-02-14 07:16:16'),
(100, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0033', '', '2019-02-14 12:47:59', '288750.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '288750.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 07:17:59', '2019-02-14 07:17:59'),
(101, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0034', '', '2019-02-14 12:49:35', '288750.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '288750.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 07:19:35', '2019-02-14 07:19:35'),
(118, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0051', '', '2019-02-14 13:26:54', '288750.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '288750.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 07:56:54', '2019-02-14 07:56:54'),
(119, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0052', '', '2019-02-14 13:27:33', '288750.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '288750.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 07:57:33', '2019-02-14 07:57:33'),
(135, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0065', '', '2019-02-14 14:50:07', '62500.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '62500.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 09:20:07', '2019-02-14 09:20:07'),
(137, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0066', '', '2019-02-14 14:52:59', '62500.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '62500.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 09:22:59', '2019-02-14 09:22:59'),
(138, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0067', '', '2019-02-14 14:54:48', '62500.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '62500.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 09:24:48', '2019-02-14 09:24:48'),
(139, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0068', '', '2019-02-14 14:55:12', '62500.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '62500.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-14 09:25:12', '2019-02-14 09:25:12'),
(168, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0095', '', '2019-02-14 17:16:16', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(170, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0097', '', '2019-02-14 17:17:43', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(171, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0098', '', '2019-02-14 17:18:02', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(172, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0099', '', '2019-02-14 17:19:09', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(173, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0100', '', '2019-02-14 17:20:06', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(174, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0101', '', '2019-02-14 17:20:10', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(175, 17, 19, NULL, NULL, NULL, 'sell', 'draft', 1, NULL, NULL, 24, NULL, 'P5yqS', '', '2019-02-15 10:53:28', '158750.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '158750.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-15 05:23:28', '2019-02-15 05:23:28'),
(176, 17, 19, NULL, NULL, NULL, 'sell', 'draft', 1, NULL, NULL, 24, NULL, 'Bbs0Z', '', '2019-02-15 10:58:44', '96250.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '96250.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-15 05:28:44', '2019-02-15 05:28:44'),
(177, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0102', '', '2019-02-15 11:00:40', '96250.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '96250.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-15 05:30:40', '2019-02-15 05:30:40'),
(178, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0103', '', '2019-02-15 11:05:53', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(179, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0104', '', '2019-02-15 11:09:01', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(183, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0105', '', '2019-02-15 11:48:34', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(184, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0106', '', '2019-02-15 11:48:46', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(185, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0107', '', '2019-02-15 11:50:19', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(186, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 24, NULL, '0108', '', '2019-02-15 11:53:12', '96250.00', NULL, '0.00', 'percentage', '0', NULL, '0.00', NULL, NULL, '96250.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, '2019-02-15 06:23:12', '2019-02-15 06:23:12'),
(187, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0109', '', '2019-02-15 12:09:48', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(188, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0110', '', '2019-02-15 12:11:02', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(189, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0111', '', '2019-02-15 12:12:46', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(190, 17, 19, NULL, NULL, NULL, 'sell', 'final', 0, 'paid', NULL, 25, NULL, '0112', '', '2019-02-15 12:13:39', '413750.00', NULL, '0.00', 'percentage', '0', 'ssh', '10.00', NULL, NULL, '413760.00', NULL, NULL, NULL, NULL, 0, '1.000', NULL, NULL, NULL, NULL, 24, NULL, 'days', NULL, '2019-02-15 06:43:39', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_payments`
--

DROP TABLE IF EXISTS `transaction_payments`;
CREATE TABLE IF NOT EXISTS `transaction_payments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) UNSIGNED DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `is_return` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Used during sales to return the change',
  `amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `method` enum('cash','card','cheque','bank_transfer','custom_pay_1','custom_pay_2','custom_pay_3','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_transaction_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_holder_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_security` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_on` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `payment_for` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_ref_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_payments_transaction_id_foreign` (`transaction_id`),
  KEY `transaction_payments_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_payments`
--

INSERT INTO `transaction_payments` (`id`, `transaction_id`, `business_id`, `is_return`, `amount`, `method`, `transaction_no`, `card_transaction_number`, `card_number`, `card_type`, `card_holder_name`, `card_month`, `card_year`, `card_security`, `cheque_number`, `bank_account_number`, `paid_on`, `created_by`, `payment_for`, `parent_id`, `note`, `payment_ref_no`, `account_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 0, '500000.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-28 11:51:39', 3, 4, NULL, NULL, 'PP2018/0001', NULL, '2018-11-28 06:21:39', '2018-11-28 06:21:39'),
(2, 2, 3, 0, '50000.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-28 11:52:47', 3, 4, NULL, NULL, 'PP2018/0002', NULL, '2018-11-28 06:22:47', '2018-11-28 06:22:47'),
(3, 5, 4, 0, '200.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-30 17:09:32', 5, 5, NULL, NULL, 'SP2018/0001', NULL, '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(4, 7, 10, 0, '1850.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-12 17:45:49', 12, 15, NULL, NULL, 'SP2018/0001', NULL, '2018-12-12 12:15:49', '2018-12-12 12:15:49'),
(44, 98, 17, 0, '288750.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 12:45:44', 24, 24, NULL, NULL, '2019/0031', NULL, '2019-02-14 07:15:44', '2019-02-14 07:15:44'),
(45, 99, 17, 0, '96250.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 12:46:16', 24, 24, NULL, NULL, 'SP2019/0032', NULL, '2019-02-14 07:16:16', '2019-02-14 07:16:16'),
(46, 100, 17, 0, '288750.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 12:47:59', 24, 24, NULL, NULL, '2019/0033', NULL, '2019-02-14 07:17:59', '2019-02-14 07:17:59'),
(47, 101, 17, 0, '288750.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 12:49:35', 24, 24, NULL, NULL, '2019/0034', NULL, '2019-02-14 07:19:35', '2019-02-14 07:19:35'),
(64, 118, 17, 0, '288750.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 13:26:54', 24, 24, NULL, NULL, '2019/0051', NULL, '2019-02-14 07:56:54', '2019-02-14 07:56:54'),
(65, 119, 17, 0, '288750.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 13:27:33', 24, 24, NULL, NULL, '2019/0052', NULL, '2019-02-14 07:57:33', '2019-02-14 07:57:33'),
(81, 135, 17, 0, '62500.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 14:50:07', 24, 24, NULL, NULL, 'SP2019/0065', NULL, '2019-02-14 09:20:07', '2019-02-14 09:20:07'),
(82, 137, 17, 0, '62500.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 14:52:59', 24, 24, NULL, NULL, 'SP2019/0066', NULL, '2019-02-14 09:22:59', '2019-02-14 09:22:59'),
(83, 138, 17, 0, '62500.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 14:54:48', 24, 24, NULL, NULL, 'SP2019/0067', NULL, '2019-02-14 09:24:48', '2019-02-14 09:24:48'),
(84, 139, 17, 0, '62500.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 14:55:12', 24, 24, NULL, NULL, 'SP2019/0068', NULL, '2019-02-14 09:25:12', '2019-02-14 09:25:12'),
(112, 168, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 17:16:16', 24, 25, NULL, NULL, '2019/0095', NULL, '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(114, 170, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 17:17:43', 24, 25, NULL, NULL, '2019/0097', NULL, '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(115, 171, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 17:18:02', 24, 25, NULL, NULL, '2019/0098', NULL, '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(116, 172, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 17:19:09', 24, 25, NULL, NULL, '2019/0099', NULL, '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(117, 173, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 17:20:06', 24, 25, NULL, NULL, '2019/0100', NULL, '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(118, 174, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-14 17:20:10', 24, 25, NULL, NULL, '2019/0101', NULL, '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(119, 177, 17, 0, '96250.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:00:40', 24, 24, NULL, NULL, 'SP2019/0102', NULL, '2019-02-15 05:30:40', '2019-02-15 05:30:40'),
(120, 178, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:05:53', 24, 25, NULL, NULL, '2019/0103', NULL, '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(121, 179, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:09:01', 24, 25, NULL, NULL, '2019/0104', NULL, '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(125, 183, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:48:34', 24, 25, NULL, NULL, '2019/0105', NULL, '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(126, 184, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:48:46', 24, 25, NULL, NULL, '2019/0106', NULL, '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(127, 185, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:50:19', 24, 25, NULL, NULL, '2019/0107', NULL, '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(128, 186, 17, 0, '96250.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 11:53:12', 24, 24, NULL, NULL, 'SP2019/0108', NULL, '2019-02-15 06:23:12', '2019-02-15 06:23:12'),
(129, 187, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 12:09:48', 24, 25, NULL, NULL, '2019/0109', NULL, '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(130, 188, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 12:11:02', 24, 25, NULL, NULL, '2019/0110', NULL, '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(131, 189, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 12:12:46', 24, 25, NULL, NULL, '2019/0111', NULL, '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(132, 190, 17, 0, '413760.00', 'cash', NULL, NULL, NULL, 'credit', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 12:13:39', 24, 25, NULL, NULL, '2019/0112', NULL, '2019-02-15 06:43:39', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_sell_lines`
--

DROP TABLE IF EXISTS `transaction_sell_lines`;
CREATE TABLE IF NOT EXISTS `transaction_sell_lines` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `variation_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(20,4) NOT NULL,
  `quantity_returned` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `unit_price_before_discount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `unit_price` decimal(20,2) DEFAULT NULL,
  `line_discount_type` enum('fixed','percentage') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line_discount_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `unit_price_inc_tax` decimal(20,2) DEFAULT NULL,
  `item_tax` decimal(20,2) DEFAULT NULL,
  `tax_id` int(10) UNSIGNED DEFAULT NULL,
  `lot_no_line_id` int(11) DEFAULT NULL,
  `sell_line_note` text COLLATE utf8mb4_unicode_ci,
  `parent_sell_line_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_sell_lines_transaction_id_foreign` (`transaction_id`),
  KEY `transaction_sell_lines_product_id_foreign` (`product_id`),
  KEY `transaction_sell_lines_variation_id_foreign` (`variation_id`),
  KEY `transaction_sell_lines_tax_id_foreign` (`tax_id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_sell_lines`
--

INSERT INTO `transaction_sell_lines` (`id`, `transaction_id`, `product_id`, `variation_id`, `quantity`, `quantity_returned`, `unit_price_before_discount`, `unit_price`, `line_discount_type`, `line_discount_amount`, `unit_price_inc_tax`, `item_tax`, `tax_id`, `lot_no_line_id`, `sell_line_note`, `parent_sell_line_id`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 2, '2.0000', '0.0000', '100.00', '100.00', 'fixed', '0.00', '100.00', '0.00', NULL, NULL, '', NULL, '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(2, 7, 3, 3, '1.0000', '0.0000', '1850.00', '1850.00', 'fixed', '0.00', '1850.00', '0.00', NULL, NULL, '', NULL, '2018-12-12 12:15:49', '2018-12-12 12:15:49'),
(83, 98, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 07:15:44', '2019-02-14 07:15:44'),
(84, 99, 7, 7, '1.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 07:16:16', '2019-02-14 07:16:16'),
(85, 100, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 07:17:59', '2019-02-14 07:17:59'),
(86, 101, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 07:19:35', '2019-02-14 07:19:35'),
(103, 118, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 07:56:54', '2019-02-14 07:56:54'),
(104, 119, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 07:57:33', '2019-02-14 07:57:33'),
(120, 135, 8, 8, '1.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 09:20:07', '2019-02-14 09:20:07'),
(122, 137, 8, 8, '1.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 09:22:59', '2019-02-14 09:22:59'),
(123, 138, 8, 8, '1.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 09:24:48', '2019-02-14 09:24:48'),
(124, 139, 8, 8, '1.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 09:25:12', '2019-02-14 09:25:12'),
(172, 168, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(173, 168, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(176, 170, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(177, 170, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(178, 171, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(179, 171, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(180, 172, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(181, 172, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(182, 173, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(183, 173, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(184, 174, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(185, 174, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(186, 175, 7, 7, '1.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 05:23:28', '2019-02-15 05:23:28'),
(187, 175, 8, 8, '1.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 05:23:28', '2019-02-15 05:23:28'),
(188, 176, 7, 7, '1.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 05:28:44', '2019-02-15 05:28:44'),
(189, 177, 7, 7, '1.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 05:30:40', '2019-02-15 05:30:40'),
(190, 178, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(191, 178, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(192, 179, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(193, 179, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(200, 183, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(201, 183, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(202, 184, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(203, 184, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(204, 185, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(205, 185, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(206, 186, 7, 7, '1.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:23:12', '2019-02-15 06:23:12'),
(207, 187, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(208, 187, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(209, 188, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(210, 188, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(211, 189, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(212, 189, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(213, 190, 7, 7, '3.0000', '0.0000', '96250.00', '96250.00', 'fixed', '0.00', '96250.00', '0.00', NULL, NULL, '', NULL, '2019-02-15 06:43:39', '2019-02-15 06:43:39'),
(214, 190, 6, 6, '2.0000', '0.0000', '62500.00', '62500.00', 'fixed', '0.00', '62500.00', '0.00', NULL, NULL, 'ppp', NULL, '2019-02-15 06:43:39', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_sell_lines_purchase_lines`
--

DROP TABLE IF EXISTS `transaction_sell_lines_purchase_lines`;
CREATE TABLE IF NOT EXISTS `transaction_sell_lines_purchase_lines` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sell_line_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'id from transaction_sell_lines',
  `stock_adjustment_line_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'id from stock_adjustment_lines',
  `purchase_line_id` int(10) UNSIGNED NOT NULL COMMENT 'id from purchase_lines',
  `quantity` decimal(20,4) NOT NULL,
  `qty_returned` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_sell_lines_purchase_lines`
--

INSERT INTO `transaction_sell_lines_purchase_lines` (`id`, `sell_line_id`, `stock_adjustment_line_id`, `purchase_line_id`, `quantity`, `qty_returned`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 4, '2.0000', '0.0000', '2018-11-30 11:39:32', '2018-11-30 11:39:32'),
(2, 2, NULL, 5, '1.0000', '0.0000', '2018-12-12 12:15:49', '2018-12-12 12:15:49'),
(35, 83, NULL, 9, '3.0000', '0.0000', '2019-02-14 07:15:44', '2019-02-14 07:15:44'),
(36, 84, NULL, 9, '1.0000', '0.0000', '2019-02-14 07:16:16', '2019-02-14 07:16:16'),
(37, 85, NULL, 9, '3.0000', '0.0000', '2019-02-14 07:17:59', '2019-02-14 07:17:59'),
(38, 86, NULL, 9, '3.0000', '0.0000', '2019-02-14 07:19:35', '2019-02-14 07:19:35'),
(55, 103, NULL, 9, '3.0000', '0.0000', '2019-02-14 07:56:54', '2019-02-14 07:56:54'),
(56, 104, NULL, 9, '3.0000', '0.0000', '2019-02-14 07:57:33', '2019-02-14 07:57:33'),
(69, 120, NULL, 10, '1.0000', '0.0000', '2019-02-14 09:20:07', '2019-02-14 09:20:07'),
(70, 122, NULL, 10, '1.0000', '0.0000', '2019-02-14 09:22:59', '2019-02-14 09:22:59'),
(71, 123, NULL, 10, '1.0000', '0.0000', '2019-02-14 09:24:48', '2019-02-14 09:24:48'),
(72, 124, NULL, 10, '1.0000', '0.0000', '2019-02-14 09:25:12', '2019-02-14 09:25:12'),
(117, 172, NULL, 9, '3.0000', '0.0000', '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(118, 173, NULL, 7, '2.0000', '0.0000', '2019-02-14 11:46:16', '2019-02-14 11:46:16'),
(121, 176, NULL, 9, '3.0000', '0.0000', '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(122, 177, NULL, 7, '2.0000', '0.0000', '2019-02-14 11:47:43', '2019-02-14 11:47:43'),
(123, 178, NULL, 9, '3.0000', '0.0000', '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(124, 179, NULL, 7, '2.0000', '0.0000', '2019-02-14 11:48:02', '2019-02-14 11:48:02'),
(125, 180, NULL, 9, '3.0000', '0.0000', '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(126, 181, NULL, 7, '2.0000', '0.0000', '2019-02-14 11:49:09', '2019-02-14 11:49:09'),
(127, 182, NULL, 9, '3.0000', '0.0000', '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(128, 183, NULL, 7, '2.0000', '0.0000', '2019-02-14 11:50:06', '2019-02-14 11:50:06'),
(129, 184, NULL, 9, '3.0000', '0.0000', '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(130, 185, NULL, 7, '2.0000', '0.0000', '2019-02-14 11:50:10', '2019-02-14 11:50:10'),
(131, 189, NULL, 9, '1.0000', '0.0000', '2019-02-15 05:30:40', '2019-02-15 05:30:40'),
(132, 190, NULL, 9, '3.0000', '0.0000', '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(133, 191, NULL, 7, '2.0000', '0.0000', '2019-02-15 05:35:53', '2019-02-15 05:35:53'),
(134, 192, NULL, 9, '3.0000', '0.0000', '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(135, 193, NULL, 7, '2.0000', '0.0000', '2019-02-15 05:39:01', '2019-02-15 05:39:01'),
(136, 200, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(137, 201, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:18:34', '2019-02-15 06:18:34'),
(138, 202, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(139, 203, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:18:46', '2019-02-15 06:18:46'),
(140, 204, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(141, 205, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:20:19', '2019-02-15 06:20:19'),
(142, 206, NULL, 9, '1.0000', '0.0000', '2019-02-15 06:23:12', '2019-02-15 06:23:12'),
(143, 207, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(144, 208, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:39:48', '2019-02-15 06:39:48'),
(145, 209, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(146, 210, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:41:02', '2019-02-15 06:41:02'),
(147, 211, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(148, 212, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:42:46', '2019-02-15 06:42:46'),
(149, 213, NULL, 9, '3.0000', '0.0000', '2019-02-15 06:43:39', '2019-02-15 06:43:39'),
(150, 214, NULL, 7, '2.0000', '0.0000', '2019-02-15 06:43:39', '2019-02-15 06:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` int(10) UNSIGNED NOT NULL,
  `actual_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allow_decimal` tinyint(1) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `units_business_id_foreign` (`business_id`),
  KEY `units_created_by_foreign` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `business_id`, `actual_name`, `short_name`, `allow_decimal`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pieces', 'Pc(s)', 0, 1, NULL, '2018-11-14 07:52:40', '2018-11-14 07:52:40'),
(2, 2, 'Pieces', 'Pc(s)', 0, 2, NULL, '2018-11-26 12:37:30', '2018-11-26 12:37:30'),
(3, 3, 'Pieces', 'Pc(s)', 0, 3, NULL, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(4, 4, 'Pieces', 'Pc(s)', 0, 5, NULL, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(6, 6, 'Pieces', 'Pc(s)', 0, 8, NULL, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(7, 7, 'Pieces', 'Pc(s)', 0, 9, NULL, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(9, 9, 'Pieces', 'Pc(s)', 0, 11, NULL, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(10, 10, 'Pieces', 'Pc(s)', 0, 12, NULL, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(11, 7, 'Tin', 'Tin', 1, 9, NULL, '2018-12-13 13:49:54', '2018-12-13 13:49:54'),
(12, 11, 'Pieces', 'Pc(s)', 0, 13, NULL, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(13, 12, 'Pieces', 'Pc(s)', 0, 14, NULL, '2018-12-28 10:56:25', '2018-12-28 10:56:25'),
(14, 13, 'Pieces', 'Pc(s)', 0, 15, NULL, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(15, 14, 'Pieces', 'Pc(s)', 0, 16, NULL, '2018-12-28 12:10:21', '2018-12-28 12:10:21'),
(16, 15, 'Pieces', 'Pc(s)', 0, 17, NULL, '2018-12-28 12:21:09', '2018-12-28 12:21:09'),
(17, 16, 'Pieces', 'Pc(s)', 0, 19, NULL, '2019-01-16 07:38:33', '2019-01-16 07:38:33'),
(18, 16, 'Number', 'No.', 0, 19, '2019-01-16 11:31:15', '2019-01-16 11:30:22', '2019-01-16 11:31:15'),
(19, 17, 'Pieces', 'Pc(s)', 0, 22, NULL, '2019-02-08 05:33:52', '2019-02-08 05:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `surname` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `contact_no` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_id` int(10) UNSIGNED DEFAULT NULL,
  `is_cmmsn_agnt` tinyint(1) NOT NULL DEFAULT '0',
  `cmmsn_percent` decimal(4,2) NOT NULL DEFAULT '0.00',
  `selected_contacts` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_business_id_foreign` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `surname`, `first_name`, `last_name`, `username`, `email`, `password`, `language`, `contact_no`, `address`, `remember_token`, `business_id`, `is_cmmsn_agnt`, `cmmsn_percent`, `selected_contacts`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Mr', 'Quadleo', 'Techno Solutios', 'superadmin', 'admin@quadleo.com', '$2y$10$ocsom0PuPxt.eVJngHM9gu2b3u5JSKcvIzxSUkmt23d7BWYf3u8my', 'en', NULL, NULL, 'AnjJTINgg0uQRDONVC3uMoYidfcsyRLTpFDRWTQlF0djprmgHLSjJyEcv8p0', 1, 0, '0.00', 0, NULL, '2018-11-14 07:52:37', '2018-11-26 12:38:52'),
(2, 'Mr.', 'Quadleo', 'Techno Solutions', 'demo', 'info@quadleo.com', '$2y$10$YvqBtx2QkXuYljHQ.bz63OS15sxvPXHN/9f0utyiCgKiv.7dGZd7.', 'en', NULL, NULL, 'pvzaoWy0LxppHsSxhamozph8d5F1OoOy4dl83d4MHnbciHbjZNB414JYwkLX', 2, 0, '0.00', 0, NULL, '2018-11-26 12:37:30', '2018-11-26 12:53:12'),
(3, 'Mr', 'Cable', 'Nine', 'cable', 'info@cablenine.com', '$2y$10$wNIwfFYSueusPL6e8xcY9OMtolV4sNC8sGpMVznzpkJWp2dqEPkx2', 'en', NULL, NULL, 'TniFhyUA86M9nDNFEkWPDlOM4Q3mkWlZFMIDxcDOPInwnTaRwwnyMRkkCWZI', 3, 0, '0.00', 0, NULL, '2018-11-27 14:32:23', '2018-11-27 14:32:23'),
(4, 'Mr', 'X', 'Y', 'xy123-03', 'abc@xyz', '$2y$10$2kBirJg1ZaRIK/QpoXCTiu9Y..9hczhzdjkvhANSXx4Y.sQD.un2u', 'en', NULL, NULL, 'LbRAJBy9E4sfHqZthNpwon3yKOhAcx4e7NVjDG4OZFWNaNyyXsEjwsn3aDvS', 3, 0, '0.00', 0, NULL, '2018-11-27 14:45:12', '2018-11-28 05:17:23'),
(5, 'Mr', 'Ashwin', 'Johny', 'ashwin', 'ashwin.johny@gmail.com', '$2y$10$HokGVe3Ko4u2CzpA7lmZiuDuC5j774KwWE4mow5loF3duKYGOho/q', 'en', NULL, NULL, 'S5bpCNaazLEtlqcRruRoNYlG5ctJXXRWkvS24HxEm2XX2JBxdmVsSBoKLtnS', 4, 0, '0.00', 0, NULL, '2018-11-28 07:25:40', '2018-11-28 07:25:40'),
(6, NULL, 'binoy', NULL, '5c0121e05a0b7', NULL, 'DUMMY', 'en', NULL, NULL, NULL, 4, 1, '10.00', 0, NULL, '2018-11-30 11:41:20', '2018-11-30 11:41:20'),
(8, 'Ms', 'Apsara', 'Bakes', 'apsara', 'apsarabakes@gmail.com', '$2y$10$Z.y6RJkQzca6SgULCg1Sw.1ABB1ofaKofDOeXjf26zA6KO.nt0QLW', 'en', NULL, NULL, NULL, 6, 0, '0.00', 0, NULL, '2018-12-03 08:16:17', '2018-12-03 08:16:17'),
(9, 'Mr', 'Nazer', 'Soudi', 'nazer', 'nazer@demo.com', '$2y$10$IJ1xQxkW4ETkSNTiQVpEqOQx884bkvQdvwLAgfVJG/p8k6QBToHdG', 'en', NULL, NULL, 'lx7MK1TrquKeCkqiQVPKUnI5O8FY2ign9FzNPPZE8Qy7bIAhf6W5Y5PY4N8l', 7, 0, '0.00', 0, NULL, '2018-12-11 07:57:42', '2018-12-11 07:57:42'),
(11, 'Mr', 'Deepu', 'KP', 'deepu', 'deepukp@gmail.com', '$2y$10$7SKsN/5ZYtX0VbVvFsYMnezrld7qUVvV68ZNLKK6Alskd11IOCs1.', 'en', NULL, NULL, NULL, 9, 0, '0.00', 0, NULL, '2018-12-11 11:59:32', '2018-12-11 11:59:32'),
(12, 'Mr', 'Shamnas', 'Thattoor', 'shamnas', 'shamnas@softfruit.solutions', '$2y$10$3e4Nvzx/YFzT6m/S.VGFP.NJPd9G76K5CqmWXUKypnO3i57WM/KWq', 'en', NULL, NULL, 'wePxJac1J0heqTlm1BXptiUAaZuvHzEwOj6mwhcyZsoi1cxmZxmxmbTXt5SA', 10, 0, '0.00', 0, NULL, '2018-12-12 13:09:05', '2018-12-12 13:09:05'),
(13, 'Mr', 'Maharoof', 'Hira', 'maharoof', 'maharoofhira@gmail.com', '$2y$10$1I2ptjp0T.VT3hjgl8MoW.GGLFAzM2WdwZKLLpW5.2I7Z/Bg/YlUW', 'en', NULL, NULL, NULL, 11, 0, '0.00', 0, NULL, '2018-12-20 09:24:09', '2018-12-20 09:24:09'),
(14, 'Mr', 'CM Jaefar', 'sadikh', 'jaefar', 'cmjsadikh313@gmail.com', '$2y$10$LP0TPxQq4S/n/TLeOEO9o.YwdXBoRZ3G7SBUL.Xizy5jNXfF3SLmq', 'en', NULL, NULL, NULL, 12, 0, '0.00', 0, NULL, '2018-12-28 10:56:24', '2018-12-28 10:56:24'),
(15, 'Mr', 'Jaefar', 'sadikh', 'jaefars', 'cmjsadikh313@gmail.com', '$2y$10$mzyPctbkso6mMaLB.fio9e6jZ8xLsxS6VkN97D21kE.1UYEMm7UR.', 'en', NULL, NULL, NULL, 13, 0, '0.00', 0, NULL, '2018-12-28 10:58:40', '2018-12-28 10:58:40'),
(16, 'Mr', 'cm jaefar', 'sadikh', 'ul@gmail.com', 'jumua@gmail.com', '$2y$10$qOHCWpVoAKk/zW8sqE1Rfuors0CytmVmCBvqs.ztOzb2tuHlawfW6', 'en', NULL, NULL, NULL, 14, 0, '0.00', 0, NULL, '2018-12-28 12:10:10', '2018-12-28 12:10:11'),
(17, 'Mr', 'jaefar', NULL, 'jaefar7', 'cmjsadikh313@gmail.com', '$2y$10$ocsom0PuPxt.eVJngHM9gu2b3u5JSKcvIzxSUkmt23d7BWYf3u8my', 'en', NULL, NULL, 'uBIBmwuGcHAO5VtheldmlMhNuSJEhDdnENQvljUYAIkPz1Sxb3lwYHOxs1TL', 15, 0, '0.00', 0, NULL, '2018-12-28 12:21:08', '2018-12-28 12:21:08'),
(18, 'mrs', 'athira', 'joobiland', 'athira-15', 'athira@joobiland.com', '$2y$10$lPnJ82YXgtqEZ065fIUKtuJP/H4LlKYhLBzIoDyM9GtKINARiwC6O', 'en', NULL, NULL, 'lajY7aguwPuLHdbNcrpDFj3uywzAIgb9yCvzEgjofUwnDWzMBqNsmQpWjQm1', 15, 0, '20.00', 0, NULL, '2018-12-29 07:31:12', '2018-12-29 07:33:53'),
(19, 'Mr', 'sadikh', 'jaefar', 'sadikhjaefar', 'sadikhjaefar@gmail.com', '$2y$10$obuc9ccYbSr/mrEK3HqYBendK8FL.K.FUP2EUTi8NOxIKA2haaHD.', 'en', NULL, NULL, 'mSSABQPIMBDunRTi6CCJP7wwXJfYQUom6d4vjq6eU9aPRfQQ5iT6WDyRMAS9', 16, 0, '0.00', 0, NULL, '2019-01-16 07:38:31', '2019-01-16 07:38:32'),
(20, 'Mr', 'user1fnm', 'user1lnm', 'user1username-16', 'user1@gmail.com', '$2y$10$YvqBtx2QkXuYljHQ.bz63OS15sxvPXHN/9f0utyiCgKiv.7dGZd7.', 'en', NULL, NULL, 'Bpxlw1hmLmwMtLedXPxz6lD9aGoVACobYPmfKXOMefChukq3q9dMUsI4LEev', 16, 0, '7.00', 0, NULL, '2019-01-16 09:20:14', '2019-01-16 09:20:14'),
(21, 'Mr', 'sadikhuserfnm', 'sadikhuserlnm', 'sadikhuser1-16', 'sadikhuser1@gmail.com', '$2y$10$o7zG058RS3XjwKlE8yzUtOGDq42p5oQ39rKVNW.uISuVyIWlMYTf2', 'en', NULL, NULL, '21z7ksU9WbRdwBAb19GNxeIq0yifTTyHBkO57BLroELdLhOCg7mL7Gtpg6Wu', 16, 0, '0.00', 0, NULL, '2019-01-16 09:51:41', '2019-01-16 10:11:22'),
(22, 'Mr', 'jaefar k', 'sadikh', 'jaeafar skilz', 'cmjsadikh313@gmail.com', '$2y$10$gxxDTNtKDi.4XfN8KXLtpOKetLfhLolubYQqPSvUMIeoXc1mVO17q', 'en', NULL, NULL, 'H6bKn95GrgOcncOyIe7OifOWt1xOplMMdgnfIwRtWatvRnzxbYBWyw4Swe0M', 17, 0, '0.00', 0, NULL, '2019-02-08 05:33:52', '2019-02-08 05:33:52'),
(23, 'Mr', 'test_skilz_user1', NULL, 'test_skilz_user1-17', 'test_skilz_user1@test.com', '$2y$10$8oxePjA4tFdOjr2dGZ5gC.fVMuQD89ro5cVPnwED1rYrOTMICGnCi', 'en', NULL, NULL, 'Sz9mpLXM2XOsyWg6dHPuzu9dJC7LWLQJMWlEucRUltWtsEXtZLucVSff1pdm', 17, 0, '0.00', 0, NULL, '2019-02-08 09:29:28', '2019-02-08 09:29:28'),
(24, 'Mr', 'safu', 'saf', 'safukt-17', 'safu@gmail.com', '$2y$10$YlpyLVbLb09YkimxdZoFlOpl4V9tDlzBjewvvO2q/VFUxzgNfkj.2', 'en', NULL, NULL, 'eRVHUkelcqTCkuIcwD7BRP45R7mcSpzUB3aPCmWDnfzoyJ32J1pvGZq3t0gQ', 17, 0, '50.00', 0, NULL, '2019-02-11 06:42:36', '2019-02-12 11:19:31'),
(25, NULL, 'skilz commisn_agnt1', 'lsnm1', '5c62a55aad737', 'skilz commisn_agnt@gmail.com', 'DUMMY', 'en', '323133323', 'tirur', NULL, 17, 1, '11.00', 0, NULL, '2019-02-12 10:52:10', '2019-02-12 11:02:55'),
(26, NULL, '2', 'sadikh', '5c62a84454816', 'asld@asd.jk', 'DUMMY', 'en', NULL, NULL, NULL, 17, 1, '7.00', 0, NULL, '2019-02-12 11:04:36', '2019-02-12 11:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `user_contact_access`
--

DROP TABLE IF EXISTS `user_contact_access`;
CREATE TABLE IF NOT EXISTS `user_contact_access` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variations`
--

DROP TABLE IF EXISTS `variations`;
CREATE TABLE IF NOT EXISTS `variations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `sub_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variation_id` int(10) UNSIGNED NOT NULL,
  `variation_value_id` int(11) DEFAULT NULL,
  `default_purchase_price` decimal(20,2) DEFAULT NULL,
  `dpp_inc_tax` decimal(20,2) NOT NULL DEFAULT '0.00',
  `profit_percent` decimal(20,2) NOT NULL DEFAULT '0.00',
  `default_sell_price` decimal(20,2) DEFAULT NULL,
  `sell_price_inc_tax` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `variations_product_id_foreign` (`product_id`),
  KEY `variations_product_variation_id_foreign` (`product_variation_id`),
  KEY `variations_name_index` (`name`),
  KEY `variations_sub_sku_index` (`sub_sku`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variations`
--

INSERT INTO `variations` (`id`, `name`, `product_id`, `sub_sku`, `product_variation_id`, `variation_value_id`, `default_purchase_price`, `dpp_inc_tax`, `profit_percent`, `default_sell_price`, `sell_price_inc_tax`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'DUMMY', 1, '0001', 1, NULL, '1500.00', '1500.00', '35.00', '2025.00', '2025.00', '2018-11-28 06:17:31', '2018-11-28 06:17:31', NULL),
(2, 'DUMMY', 2, '0002', 2, NULL, '80.00', '80.00', '25.00', '100.00', '100.00', '2018-11-30 11:36:36', '2018-11-30 11:36:36', NULL),
(3, 'DUMMY', 3, 'A2A', 3, NULL, '1785.00', '1785.00', '3.64', '1850.00', '1850.00', '2018-12-12 12:11:06', '2018-12-12 12:11:06', NULL),
(4, 'DUMMY', 4, '0004', 4, NULL, '725.00', '725.00', '7.59', '780.00', '780.00', '2018-12-12 12:20:39', '2018-12-12 12:20:39', NULL),
(5, 'DUMMY', 5, '0005', 5, NULL, '3000.00', '3000.00', '25.00', '3750.00', '3750.00', '2019-01-25 09:17:16', '2019-01-25 09:17:16', NULL),
(6, 'DUMMY', 6, '0006', 6, NULL, '50000.00', '52500.00', '25.00', '62500.00', '65625.00', '2019-02-08 06:04:50', '2019-02-08 06:04:50', NULL),
(7, 'DUMMY', 7, '0007', 7, NULL, '77000.00', '77000.00', '25.00', '96250.00', '96250.00', '2019-02-12 05:30:18', '2019-02-12 09:17:20', NULL),
(8, 'DUMMY', 8, '0008', 8, NULL, '50000.00', '50000.00', '25.00', '62500.00', '62500.00', '2019-02-12 05:31:20', '2019-02-12 09:17:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `variation_group_prices`
--

DROP TABLE IF EXISTS `variation_group_prices`;
CREATE TABLE IF NOT EXISTS `variation_group_prices` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variation_id` int(10) UNSIGNED NOT NULL,
  `price_group_id` int(10) UNSIGNED NOT NULL,
  `price_inc_tax` decimal(20,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `variation_group_prices_variation_id_foreign` (`variation_id`),
  KEY `variation_group_prices_price_group_id_foreign` (`price_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variation_location_details`
--

DROP TABLE IF EXISTS `variation_location_details`;
CREATE TABLE IF NOT EXISTS `variation_location_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_variation_id` int(10) UNSIGNED NOT NULL COMMENT 'id from product_variations table',
  `variation_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `qty_available` decimal(20,4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `variation_location_details_location_id_foreign` (`location_id`),
  KEY `variation_location_details_product_id_index` (`product_id`),
  KEY `variation_location_details_product_variation_id_index` (`product_variation_id`),
  KEY `variation_location_details_variation_id_index` (`variation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variation_location_details`
--

INSERT INTO `variation_location_details` (`id`, `product_id`, `product_variation_id`, `variation_id`, `location_id`, `qty_available`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 3, '1000.0000', '2018-11-28 06:21:39', '2018-11-28 06:21:39'),
(2, 1, 1, 1, 4, '100.0000', '2018-11-28 06:22:47', '2018-11-28 06:22:47'),
(3, 2, 2, 2, 5, '11.0000', '2018-11-30 11:36:58', '2018-11-30 11:36:58'),
(4, 2, 2, 2, 6, '10.0000', '2018-11-30 11:36:58', '2018-11-30 11:39:32'),
(5, 3, 3, 3, 12, '0.0000', '2018-12-12 12:13:42', '2018-12-12 12:15:49'),
(6, 5, 5, 5, 2, '10.0000', '2019-01-25 09:17:32', '2019-01-25 09:17:32'),
(7, 6, 6, 6, 19, '73.0000', '2019-02-08 06:06:06', '2019-02-15 06:43:39'),
(8, 7, 7, 7, 19, '37.0000', '2019-02-12 09:17:35', '2019-02-15 06:43:39'),
(9, 8, 8, 8, 19, '46.0000', '2019-02-12 09:17:50', '2019-02-15 05:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `variation_templates`
--

DROP TABLE IF EXISTS `variation_templates`;
CREATE TABLE IF NOT EXISTS `variation_templates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `variation_templates_business_id_foreign` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variation_value_templates`
--

DROP TABLE IF EXISTS `variation_value_templates`;
CREATE TABLE IF NOT EXISTS `variation_value_templates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variation_template_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `variation_value_templates_name_index` (`name`),
  KEY `variation_value_templates_variation_template_id_index` (`variation_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barcodes`
--
ALTER TABLE `barcodes`
  ADD CONSTRAINT `barcodes_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `brands_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `business`
--
ALTER TABLE `business`
  ADD CONSTRAINT `business_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `business_default_sales_tax_foreign` FOREIGN KEY (`default_sales_tax`) REFERENCES `tax_rates` (`id`),
  ADD CONSTRAINT `business_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `business_locations`
--
ALTER TABLE `business_locations`
  ADD CONSTRAINT `business_locations_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `business_locations_invoice_layout_id_foreign` FOREIGN KEY (`invoice_layout_id`) REFERENCES `invoice_layouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `business_locations_invoice_scheme_id_foreign` FOREIGN KEY (`invoice_scheme_id`) REFERENCES `invoice_schemes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cash_registers`
--
ALTER TABLE `cash_registers`
  ADD CONSTRAINT `cash_registers_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cash_registers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cash_register_transactions`
--
ALTER TABLE `cash_register_transactions`
  ADD CONSTRAINT `cash_register_transactions_cash_register_id_foreign` FOREIGN KEY (`cash_register_id`) REFERENCES `cash_registers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contacts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD CONSTRAINT `customer_groups_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD CONSTRAINT `expense_categories_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `group_sub_taxes`
--
ALTER TABLE `group_sub_taxes`
  ADD CONSTRAINT `group_sub_taxes_group_tax_id_foreign` FOREIGN KEY (`group_tax_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `group_sub_taxes_tax_id_foreign` FOREIGN KEY (`tax_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_layouts`
--
ALTER TABLE `invoice_layouts`
  ADD CONSTRAINT `invoice_layouts_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_schemes`
--
ALTER TABLE `invoice_schemes`
  ADD CONSTRAINT `invoice_schemes_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `printers`
--
ALTER TABLE `printers`
  ADD CONSTRAINT `printers_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_tax_foreign` FOREIGN KEY (`tax`) REFERENCES `tax_rates` (`id`),
  ADD CONSTRAINT `products_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD CONSTRAINT `product_variations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_lines`
--
ALTER TABLE `purchase_lines`
  ADD CONSTRAINT `purchase_lines_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `purchase_lines_tax_id_foreign` FOREIGN KEY (`tax_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `purchase_lines_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `purchase_lines_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `variations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `res_product_modifier_sets`
--
ALTER TABLE `res_product_modifier_sets`
  ADD CONSTRAINT `res_product_modifier_sets_modifier_set_id_foreign` FOREIGN KEY (`modifier_set_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `res_tables`
--
ALTER TABLE `res_tables`
  ADD CONSTRAINT `res_tables_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `selling_price_groups`
--
ALTER TABLE `selling_price_groups`
  ADD CONSTRAINT `selling_price_groups_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stock_adjustment_lines`
--
ALTER TABLE `stock_adjustment_lines`
  ADD CONSTRAINT `stock_adjustment_lines_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stock_adjustment_lines_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stock_adjustment_lines_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `variations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD CONSTRAINT `subscriptions_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tax_rates`
--
ALTER TABLE `tax_rates`
  ADD CONSTRAINT `tax_rates_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tax_rates_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_expense_category_id_foreign` FOREIGN KEY (`expense_category_id`) REFERENCES `expense_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_expense_for_foreign` FOREIGN KEY (`expense_for`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `business_locations` (`id`),
  ADD CONSTRAINT `transactions_tax_id_foreign` FOREIGN KEY (`tax_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transaction_payments`
--
ALTER TABLE `transaction_payments`
  ADD CONSTRAINT `transaction_payments_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transaction_sell_lines`
--
ALTER TABLE `transaction_sell_lines`
  ADD CONSTRAINT `transaction_sell_lines_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transaction_sell_lines_tax_id_foreign` FOREIGN KEY (`tax_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transaction_sell_lines_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transaction_sell_lines_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `variations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `units`
--
ALTER TABLE `units`
  ADD CONSTRAINT `units_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `units_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `variations`
--
ALTER TABLE `variations`
  ADD CONSTRAINT `variations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `variations_product_variation_id_foreign` FOREIGN KEY (`product_variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `variation_group_prices`
--
ALTER TABLE `variation_group_prices`
  ADD CONSTRAINT `variation_group_prices_price_group_id_foreign` FOREIGN KEY (`price_group_id`) REFERENCES `selling_price_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `variation_group_prices_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `variations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `variation_location_details`
--
ALTER TABLE `variation_location_details`
  ADD CONSTRAINT `variation_location_details_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `business_locations` (`id`),
  ADD CONSTRAINT `variation_location_details_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `variations` (`id`);

--
-- Constraints for table `variation_templates`
--
ALTER TABLE `variation_templates`
  ADD CONSTRAINT `variation_templates_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `variation_value_templates`
--
ALTER TABLE `variation_value_templates`
  ADD CONSTRAINT `variation_value_templates_variation_template_id_foreign` FOREIGN KEY (`variation_template_id`) REFERENCES `variation_templates` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
